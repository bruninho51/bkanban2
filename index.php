<?php

require_once 'vendor/autoload.php';

/*
ini_set("display_errors", 1);
ini_set("track_errors", 1);
ini_set("html_errors", 1);
error_reporting(E_ALL);
*/
session_start();

$app = new Slim\App([
    'settings' => [
        'displayErrorDetails' => true,
        'blade' => [
            'blade_template_path' => __DIR__ . "/Services/Application/Public/",
            'blade_cache_path' => __DIR__ . "/Services/Infraestructure/Storage/BladeCache/"
        ]
    ],
]);

$container = $app->getContainer();

$container['flash'] = function () {
    return new \Slim\Flash\Messages();
};

//Registrando Blade no CI do Slim
$container['view'] = function ($container) {
    return new \Slim\Views\Blade(
        $container['settings']['blade']['blade_template_path'],
        $container['settings']['blade']['blade_cache_path']
    );
};

//Percorrendo controladores e colocando-os no CI do Slim
foreach (new FileSystemIterator(__DIR__ . '/Services/Application/Controller') as $controller) {
    $container[$controller->getBaseName('.php')] = function ($container) use ($controller){
        $namespace = "\\Services\\Application\\Controller\\" . (string) $controller->getBaseName('.php');
        $ctl = new $namespace($container);

        return $ctl;
    };
}

require_once 'Services/Application/Routes/routes.php';

$app->run();
