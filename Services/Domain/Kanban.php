<?php

namespace Services\Domain;

class Kanban extends Model{
    
    const TABLE = 'kanbans';
    
    public $fillable;
    
    public function __construct()
    {
        $this->fillable = ['name', 'users_email', 'createdAt', 'updatedAt', 'deletedAt'];
        $this->fieldsToShow = ['id', 'name', 'users_email', 'createdAt', 'updatedAt', 'deletedAt'];
        $this->optional = ['updatedAt', 'deletedAt', 'createdAt'];
    }

    public function __isset($prop)
    {
        return isset($this->data[$prop]);
    }
    
    public function __get($prop)
    {
        return $this->data[$prop];
    }
    
    public function __set($prop, $value)
    {
        parent::__set($prop, $value);
        switch ($prop) {
            case 'id':
                $this->data['id'] = trim($value);
                break;
            case 'name':
                $this->data['name'] = trim($value);
                break;
            case 'tasks':
                $this->data['tasks'] = $value;
                break;
            case 'users_email':
                $this->data['users_email'] = $value;
                break;
        }
    }

    /**
     * Serve para criar um identificador para o HTML
     * começa com identity_ porque, caso o nome do kanban comece com um número, não bugará o JavaScript
     * o id é concatenado no final para não dar conflito caso um kanban se chame HelloWorld e outro Hello World
     * 
     * @return string
     */
    public function getIdentity()
    {
        return 'identity_' . str_replace(' ', '', $this->name) . $this->id;
    }
}
