<?php

namespace Services\Domain;
use Exception;

class Task extends Model{

    const COLOR_DEFAULT = '#0059C9';
    const TO_DO = 1;
    const WORKING_IN_PROGRESS = 2;
    const DONE = 3;
    const TABLE = 'tasks';
    const INVALID_USER_OBJECT   = 'User must receive a valid user object!';
    const INVALID_KANBAN_OBJECT = 'Kanban must receive a valid kanban object!';
    const MSG_TASK_FILED = 'Task successfully archived!';
    
    public function __construct()
    {
        $this->fillable = [
            'name', 'description', 'list', 'users_email', 'position', 'filed', 'color', 'kanbans_id', 'createdAt', 'updatedAt', 'deletedAt'
        ];

        $this->optional = [
            'kanbans_id', 'updatedAt', 'deletedAt', 'createdAt'
        ];

        $this->fieldsToShow = [
            'id', 'name', 'description', 'list', 'users_email', 'position', 'filed', 'color', 'createdAt', 'updatedAt', 'deletedAt'
        ];
    }

    public function __isset($prop)
    {
        return isset($this->data[$prop]);
    }

    public function __get($prop)
    {
        return $this->data[$prop];
    }
    
    public function __set($prop, $value)
    {
        parent::__set($prop, $value);
        switch ($prop) {
            case 'id':
                $this->data['id'] = trim($value);
                break;
            case 'name':
                $this->data['name'] = trim($value);
                break;
            case 'description':
                $this->data['description'] = trim($value);
                break;
            case 'list':
                $this->data['list'] = trim($value);
                break;
            case 'position':
                $this->data['position'] = trim($value);
                break;
            case 'filed':
                $this->data['filed'] = trim($value);
                break;
            case 'user':
                if (get_class($value) == get_class(new User)) {
                    $this->data['user'] = $value;
                } else {
                    throw new Exception(self::INVALID_USER_OBJECT);
                }
                break;
            case 'color':
                $this->data['color'] = trim($value);
                break;
            case 'users_email':
                $this->data['users_email'] = trim($value);
                break;
            case 'kanbans_id':
                $this->data['kanbans_id'] = trim($value);
                break;
            case 'kanban':
                if (get_class($value) == get_class(new Kanban)) {
                    $this->data['kanban'] = $value;
                } else {
                    die();
                    throw new Exception(self::INVALID_KANBAN_OBJECT);
                }
                break;
        }
    }

    public function getDescriptionResume()
    {
        $len = strlen($this->description);

        if ($len < 100) {
            return $this->description;
        }

        return substr($this->description, 0, 100) . '...';
    }

    public function getNameResume()
    {
        $len = strlen($this->name);

        if ($len < 20) {
            return $this->name;
        }

        return substr($this->name, 0, 20) . '...';
    }

    public function toArray()
    {
        $data = parent::toArray();
        $data['nameResume']        = $this->getNameResume();
        $data['descriptionResume'] = $this->getDescriptionResume();

        return $data;
    }
}
