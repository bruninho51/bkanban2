<?php


namespace Services\Domain;

use Cassandra\Date;
use DateTime;
use mysql_xdevapi\Exception;
use Services\Infraestructure\Persistence\TaskMapper;

class Timer extends Model
{
    const TABLE = 'timers';
    const INVALID_DATE = 'Invalid Date Error';
    const INVALID_ENTITY_VALUE = 'Entity must be an instance of Model';
    const NON_COMPLIANT_ENTITY = 'Informed entity not compatible with timer';
    const INVALID_INFORMATION = 'Missing data or invalid data entered.';

    public function __construct()
    {
        $this->fillable = ['foreign_key', 'entity', 'start', 'stop', 'createdAt', 'updatedAt', 'deletedAt'];
        $this->fieldsToShow = ['id', 'start', 'stop', 'foreign_key', 'entity', 'createdAt', 'updatedAt', 'deletedAt'];
        $this->optional = ['createdAt', 'updatedAt', 'deletedAt'];
    }

    public function __isset($prop)
    {
        return isset($this->data[$prop]);
    }

    public function __get($prop)
    {
        return $this->data[$prop];
    }

    public function __set($prop, $value)
    {
        parent::__set($prop, $value);
        switch ($prop) {
            case 'id':
            case 'foreign_key':
                $this->data[$prop] = trim($value);
                break;
            case 'start':
            case 'stop':
                if (is_numeric($value)) {
                    $this->data[$prop] = $value;
                } else {
                    throw new Exception(self::INVALID_DATE);
                }
                break;
            case 'entity':
                $this->data['entityModel'] = $this->getInstanceOfEntity($value);
                $this->data['entity'] = $value;
                break;
        }
    }

    public function getInstanceOfEntity($entity) : Model
    {
        switch (strtolower($entity)) {
            case strtolower('Tasks'):
                if (isset($this->foreign_key) && $this->foreign_key)
                    return TaskMapper::findById($this->foreign_key);

                return new Task();
                break;
            case strtolower('TimerLists'):
                /*
                if (isset($this->id) && $this->id)
                    return new TaskList();

                return new TaskList();
                */
                break;

            throw new Exception(self::NON_COMPLIANT_ENTITY);
        }
    }
}