<?php

namespace Services\Domain;

class User extends Model{
    
    const TABLE = 'users';
    
    public $fillable;
    
    public function __construct()
    {
        $this->fillable = [
            'name', 'email', 'password'
        ];
        $this->fieldsToShow = [
            'name', 'email'
        ];
    }
    
    public function __get($prop)
    {
        return $this->data[$prop];
    }
    
    public function __set($prop, $value)
    {
        switch ($prop) {
            case 'id':
                $this->data['id'] = trim($value);
                break;
            case 'name':
                $this->data['name'] = trim($value);
                break;
            case 'email':
                $this->data['email'] = trim($value);
                break;
            case 'password':
                $this->data['password'] = trim($value);
                break;
        }
    }
}
