<?php

namespace Services\Domain;

abstract class Model implements Contracts\Model{
    const MSG_CAD_SUCCESS  = 'Successful registration!';
    const MSG_EDIT_SUCCESS = 'successfully edited!';
    const MSG_DATA_MISING  = 'Please enter all necessary data!';

    protected $data;
    protected $fieldsToShow;
    protected $optional;
    protected $fillable;

    public function __set($prop, $value)
    {
        switch ($prop) {
            case 'createdAt':
                $this->data['createdAt'] = $value;
                break;
            case 'updatedAt':
                $this->data['updatedAt'] = $value;
                break;
            case 'deletedAt':
                $this->data['deletedAt'] = $value;
                break;
        }
    }

    public function getFillable()
    {
        return $this->fillable;
    }

    public function getFieldsToShow()
    {
        return $this->fieldsToShow;
    }

    public function getOptionalFields()
    {
        return $this->optional;
    }

    public function toArray()
    {
        return $this->data;
    }
}
