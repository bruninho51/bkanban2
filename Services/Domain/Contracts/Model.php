<?php

namespace Services\Domain\Contracts;

interface Model {
    public function getFillable();
    public function getFieldsToShow();
    public function getOptionalFields();
    public function toArray();
}
