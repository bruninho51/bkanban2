<?php

namespace Services\Application\Middleware;

class Auth extends Middleware{
    const MSG_SESSION_EXPIRED = 'Session has been expired!';
    public function __invoke($request, $response, $next) {
        
        if (!isset($_SESSION['user'])) {
            if ($this->isXmlHttpRequest($request)) {
                return $response->withJson([
                    'title' =>  'Error',
                    'type' =>   'error',
                    'content' => self::MSG_SESSION_EXPIRED
                ], 401);
            }
            return $response->withRedirect($this->router->pathFor('login'));
        }
        
        $response = $next($request, $response);
        return $response;
    }

    function isXmlHttpRequest($request)
    {
        $isAjax = $request->getHeaderLine('HTTP_X_REQUESTED_WITH');
        return (strtolower($isAjax) === 'xmlhttprequest');
    }
}
