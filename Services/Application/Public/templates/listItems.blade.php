@if(isset($tasks) && !empty($tasks))
    @foreach($tasks as $task)
        <li data-code="{{$task->id}}" class="list-item" data-position="{{$task->position}}" data-color="{{$task->color}}" style="background-color: {{$task->color}}">
            <span>
                <strong data-toggle="popover" class="name" data-container="body" data-content="{{$task->name}}">{{$task->getNameResume()}}</strong><br>
                <p data-toggle="popover" class="description" data-container="body" data-content="{{$task->description}}">{{$task->getDescriptionResume()}}</p>
            </span>
            <i class="item-delete fa fa-close" onclick="Kanban.filedTask(this)"></i>
            <i class="item-clock fa fa-clock-o" onclick="TimerList.start({{$task->id}}, 'tasks', '{{$task->getNameResume()}}')"></i>
        </li>
    @endforeach
@endif