<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="shortcut icon" href="/Services/Application/Public/Resources/Images/favicon.ico">
        <title>{{ $title }}</title>
        <link rel="stylesheet" href="/Services/Application/Public/Resources/CSS/principal.css">
        <link rel="stylesheet" href="/Services/Application/Public/Lib/hint.css-2.5.1/hint.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="/Services/Application/Public/Lib/jQueryUI1.12.1/jquery-ui.css">
        <link rel="stylesheet" href="/Services/Application/Public/Lib/jQueryConfirm/jQueryConfirm.min.css">
        <link rel="stylesheet" href="/Services/Application/Public/Lib/bootstrap4.3.1/css/bootstrap.css">
        <link rel="stylesheet" href="/Services/Application/Public/Lib/ContextMenu/css/ContextMenu.css">
        <link rel="stylesheet" href="/Services/Application/Public/Lib/TaskEditor/css/TaskEditor.css">
        <link rel="stylesheet" href="/Services/Application/Public/Lib/TimerList/css/TimerList.css">
        @if(isset($stylesheets))
            @foreach($stylesheets as $css)
                <link rel="stylesheet" href="/Services/Application/Public/Resources/CSS/{{ $css }}.css">
            @endforeach
        @endif
    </head>
    <body>
        @yield('navbar')
        
        <div class="container">
            @yield('content')
        </div>

        <script src="/Services/Application/Public/Lib/jQuery/jQuery.js"></script>
        <script src="/Services/Application/Public/Lib/Moment/moment.js"></script>
        <script src="/Services/Application/Public/Lib/jQueryUI1.12.1/jquery-ui.js"></script>
        <script src="/Services/Application/Public/Lib/popper/Popper.js"></script>
        <script src="/Services/Application/Public/Lib/jQueryConfirm/jQueryConfirm.min.js"></script>
        <script src="/Services/Application/Public/Lib/bootstrap4.3.1/js/bootstrap.bundle.min.js"></script>
        <script src="/Services/Application/Public/Lib/bootstrap4.3.1/js/bootstrap.js"></script>
        <script src="/Services/Application/Public/Lib/jquery.nicescroll-3.7.6/jquery.nicescroll.min.js"></script>
        <script src="/Services/Application/Public/Lib/TimerList/TimerList.js"></script>
        <script src="/Services/Application/Public/Lib/Principal.js"></script>
        <script src="/Services/Application/Public/Lib/ContextMenu/ContextMenu.js"></script>
        <script src="/Services/Application/Public/Lib/TaskEditor/js/TaskEditor.js"></script>
        <script src="/Services/Application/Public/Lib/jQueryMask/jquery.mask.min.js"></script>
        @if(isset($scripts))
            @foreach($scripts as $js)
                <script type="text/javascript" src="/Services/Application/Public/Lib/{{ $js }}.js"></script>
            @endforeach
        @endif

        @if(isset($messages['alertSuccess']))
            <script>Principal.alert("Success", '{{ current($messages['alertSuccess']) }}', 'success')</script>
        @endif
        @if(isset($messages['alertDanger']))
            <script>Principal.alert("Error", '{{ current($messages['alertDanger']) }}', 'danger')</script>
        @endif
        @yield('modal-timer-list')
    </body>
</html>