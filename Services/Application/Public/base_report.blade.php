@section('content')
    <h1 class="title">{{$title}}</h1>
    <div class="filters">
        {!! $filters->mount() !!}
    </div>
@endsection

@include('navbar')
@include('templates.template')
