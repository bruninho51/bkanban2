@section('content')
<form action="login" method="post" class="login-box">
    <img class="ico-login" src="/Services/Application/Public/Resources/Images/icon_black.png">
    <div class="form-group">
        <label for="email">E-mail:</label>
        <input type="email" name="email" class="form-control" id="email">
    </div>
    <div>
        <label for="password">Password:</label>
        <input type="password" name="password" class="form-control" id="password">
    </div>
    <small class="form-text text-danger">
        @if (isset($messages['error']))
            <span class="error">{{current($messages['error'])}}</span><br>
        @endif
    </small>
    <div class="form-group">
        <br>
        <button type="submit" class="btn btn-block btn-primary">Login</button>
    </div>
</form>
@endsection

@include('templates.template')