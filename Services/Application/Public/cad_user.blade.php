@section('content')
    <form action="/users/register" method="post" class="container-form">
        <h2>Register User</h2>
        <div class="form-group">
            <label for="name">Name: </label>
            <input type="text" name="name" class="form-control">
        </div>
        <div class="form-group">
            <label for="email">Email: </label>
            <input type="email" name="email" class="form-control">
        </div>
        <div class="form-group">
            <label for="password">Password: </label>
            <input type="password" name="password" class="form-control">
        </div>
        <small class="form-text text-danger">
            @if (isset($messages['error']))
                <span class="error">{{current($messages['error'])}}</span><br>
            @endif
        </small>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </form>
@endsection

@include('navbar')
@include('templates.template')
