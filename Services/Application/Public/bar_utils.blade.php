@section('bar_utils')
    <div id="barUtils">
        <div class="row" style="height: 100%">
            <div class="col-6 d-flex justify-content-start align-items-center">
                <!--<button id="btnTimerList" class="btn btn-success" onclick="TimerList.showModal()">Time List</button>-->
                <span id="statusTimer"></span>
                <span id="timeDisplay"></span>
                <button id="btnStopTimer" disabled class="btn btn-danger fa fa-stop" onclick="TimerList.stop()"></button>
                <input id="time" type="text" disabled >
                <!-- <input id="start" type="text" disabled > 
                <input id="stop" type="text" disabled > -->
            </div>
            <div class="col-6 d-flex justify-content-end align-items-center">
                <button class="btn btn-success" onclick="Kanban.showInputKanban()">New Kanban</button>
            </div>
        </div>
    </div>
@endsection