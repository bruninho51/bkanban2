var TaskEditor = {
    open: function (taskCode) {
        document.getElementById('backgroundTaskEditor').style.display = 'block';
        var taskEditor = document.getElementById('taskEditor');
        taskEditor.style.display = 'block';
        taskEditor.dataset.code = taskCode;
        var task = document.querySelector('li[data-code="' + taskCode + '"]');
        var coords = task.getBoundingClientRect();
        taskEditor.style.left = coords.left + 'px';
        taskEditor.style.top = coords.top + 'px';
        Kanban.getTask(taskCode, function (taskDB) {
            taskEditor.children[0].children[0].value = taskDB.name;
            taskEditor.children[0].children[1].value = taskDB.description;
        });
    },
    close: function () {
        document.getElementById('backgroundTaskEditor').style.display = 'none';
        document.getElementById('taskEditor').style.display = 'none';
        TaskEditor.removeAllSpinners();
    },
    create: function () {
        var background = document.createElement('div');
        background.style.backgroundColor = 'rgba(0,0,0,0.5)';
        background.style.position = 'absolute';
        background.style.top = '0px';
        background.style.left = '0px';
        background.style.width = '100%';
        background.style.height = '100%';
        background.style.display = 'none';
        background.id = 'backgroundTaskEditor';
        background.addEventListener('click', function () {
            TaskEditor.close();
        });

        var taskEditor = document.createElement('div');
        taskEditor.id = 'taskEditor';
        taskEditor.style.display = 'none';
        taskEditor.style.position = 'absolute';
        taskEditor.style.left = '0px';
        taskEditor.style.top = '0px';

        var form = document.createElement('form');

        var inputName = document.createElement('input');
        inputName.setAttribute('type', 'text');
        inputName.setAttribute('name', 'name');
        inputName.setAttribute('class', 'form-control');
        inputName.style.marginBottom = '5px';

        var inputDescription = document.createElement('textarea');
        inputDescription.setAttribute('name', 'description');
        inputDescription.setAttribute('class', 'form-control');
        inputDescription.style.marginBottom = '5px';

        var submit = document.createElement('submit');
        submit.setAttribute('type', 'button');
        submit.setAttribute('class', 'btn btn-success float-right');
        submit.setAttribute('id', 'taskEditorSaveButton');
        submit.textContent = 'Save';
        submit.addEventListener('click', () => this.saveModifications());

        form.appendChild(inputName);
        form.appendChild(inputDescription);
        form.appendChild(submit);

        taskEditor.appendChild(form);

        document.body.appendChild(background);
        document.body.appendChild(taskEditor);

        //Evento para abrir o ContextMenu
        $(document).delegate('.list-item', 'contextmenu', function (event){
            ContextMenu.open(event);
            event.preventDefault();
        });

        //Seta evento no DOM para que, ao clicar fora do ContextMenu, este seja fechado
        document.addEventListener('mousedown', function(event){
            if ($(event.target).closest('#contextMenu').length == 0) {
                ContextMenu.close();
            }
        });
    },
    saveModifications: function () {
        let taskEditor = document.getElementById('taskEditor');
        let taskCode = taskEditor.dataset.code;
        Kanban.getTask(taskCode, function (taskDB) {
            let name = taskEditor.children[0].children[0].value;
            let description = taskEditor.children[0].children[1].value;
            TaskEditor.showLoading();
            Kanban.editTask(taskCode, name, description, taskDB.list, {
                200: function (resp) {
                    let task = document.querySelector('li[data-code="' + resp.task.id + '"]');
                    let name = task.getElementsByClassName('name')[0];
                    name.textContent = resp.task.nameResume;
                    name.dataset.content = resp.task.name; //Popup
                    let description = task.getElementsByClassName('description')[0];
                    description.textContent = resp.task.descriptionResume;
                    description.dataset.content = resp.task.description; //Popup'
                    TaskEditor.close();
                }
            });
        });
    },
    getLoadingSpinner: function () {
        let spinner = document.createElement('span');
        spinner.setAttribute('class', 'spinner-border spinner-border-sm spinner-taskEditor');
        spinner.setAttribute('role', 'status');
        spinner.setAttribute('aria-hidden', 'true');

        return spinner;
    },
    removeAllSpinners: function () {
        let spinners = document.getElementsByClassName('spinner-taskEditor');
        for (let spinner of spinners) {
            spinner.remove();
        }
    },
    showLoading: function () {
        let saveButton = document.getElementById('taskEditorSaveButton');
        saveButton.prepend(TaskEditor.getLoadingSpinner());
    }
};