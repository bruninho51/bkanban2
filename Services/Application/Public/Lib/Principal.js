var Principal = {
    alert: function (title, content, type, callback = false) {
        var color, icon;
        if (type == 'success') {
            color = 'blue';
            icon = 'fa fa-check';
        } else if (type == 'danger') {
            color = 'red';
            icon = 'fa fa-warning';
        } else if (type == 'orange') {
            color = 'orange';
            icon = 'fa fa-warning';
        }
        $.alert({
            title: title,
            content: content,
            type: color,
            icon: icon,
            buttons: {
                ok: function () {
                    if (callback)
                        callback();
                }
            }
        });
    },
    newRequest: function (url, method, dataType, params, callbacks = {}) {
        let config = {
            url: url,
            method: method,
            dataType: dataType,
            statusCode: {
                401: function () {
                    Principal.alert(
                        'Access Forbidden', 
                        'Authentication required. Please login!', 
                        'orange', 
                        () => location.reload()
                    );
                }
            }
        };
        Object.assign(config.statusCode, callbacks);
        if (Object.keys(params).length > 0) {
            config.data = params;
        }
        return $.ajax(config);
    },
    redirect: function (url) {
        window.location.href = url;
    },
    downloadXLSX: function(uri, params) {
        let form = new FormData();
        Object.entries(params).map(function(param) {
            form.append(param[0], String(param[1]));
        });

        var request = new XMLHttpRequest();
        request.open('POST', uri, true);
        request.responseType = 'blob';

        request.onload = function(e) {
            if (this.status === 200) {
                var blob = this.response;
                if(window.navigator.msSaveOrOpenBlob) {
                    window.navigator.msSaveBlob(blob, fileName);
                }
                else{
                    var downloadLink = window.document.createElement('a');
                    var contentTypeHeader = request.getResponseHeader("Content-Type");
                    downloadLink.href = window.URL.createObjectURL(new Blob([blob], { type: contentTypeHeader }));
                    downloadLink.download = 'arquivo.xlsx';
                    document.body.appendChild(downloadLink);
                    downloadLink.click();
                    document.body.removeChild(downloadLink);
                }
            }
        };
        request.send(form);
    },
    slideText: function (message, elementId, pause = 0) {
        var MESSAGE = "       " + message + "       ";
        var SPEED   = 10;
        var id, position = 0;
        function action() {
            if (!pause) {
                clearTimeout(id);
                pause = 1;
            } else {
                Principal.banner(MESSAGE, SPEED, position, elementId);
                pause = 0;
            }
        }
        Principal.banner(MESSAGE, SPEED, position, elementId);
    },
    stopSlideText: function (elementId) {
        Principal.slideText('', elementId, 1);
    },
    banner: function (MESSAGE, SPEED, position, elementId) {
        let i, k, msg = MESSAGE;
        k= (66/msg.length) + 1;
        for (i=0; i<=k; i++) msg += " " + msg;
        document.getElementById(elementId).innerText = msg.substring(position, position + 95); // Aumentando ou diminuindo o tamnaho do banner este numero deverá ser auterado.
        if (position ++== msg.length /*document.form1.message.value.length*/) position = 0;
        let fBanner = function () {
            Principal.banner(MESSAGE, SPEED, position, elementId);
        };
        id = setTimeout(fBanner,1000/SPEED);
    },
    scroll: function (element) {
        $(element).niceScroll({
            cursorwidth:10,
            cursoropacitymin: 1,
            cursorcolor: '#6e8cb6',
            cursorborder: 'none',
            cursorborderradius: 4,
            autohidemode: 'leave'    
        });
    },
    init: function () {
        if (Kanban !== undefined)
            Kanban.init();
        if (TimerList !== undefined)
            TimerList.init();
    }
};

$(document).ready(function () {
   Principal.init(); 
});