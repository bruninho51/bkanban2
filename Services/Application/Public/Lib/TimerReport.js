var Validators = {
  validatePeriod: function(filters) {
      let insertedPeriod = filters.start && filters.stop;
      if(insertedPeriod) {
          let patternData = /^[0-9]{4}-[0-9]{2}-[0-9]{2}$/;
          let invalidDate = !patternData.test(filters.start) || !patternData.test(filters.stop);
          if(invalidDate){
              return ['Invalid date.'];
          }
          let start = moment(filters.start);
          let stop = moment(filters.stop);
          if(start > stop) {
              return ['Start date must not be greater than end date.'];
          }
      } else {
          return ['Invalid period.']
      }

      return [];
  }
};

BaseReport.setValidator(function(filters) {
    let errors = [];
    let validators = [
      Validators.validatePeriod
    ];
    for(let validator of validators) {
        errors = validator(filters);
        if(errors) {
            break;
        }
    }

    return errors;
});
