var ContextMenu = {
    //colors: new Array('#5E2381', '#28E281', '#EA197F', '#F4F17E', '#E8967E', '#0059C9'),
    colors: new Array('#00A0B0', '#6A4A3C', '#CC333F', '#EB6841', '#EDC951', '#0059C9'),
    open: function (event) {
        var menu = document.getElementById('contextMenu');
        menu.style.display = 'block';
        menu.style.left = event.clientX + 'px';
        menu.style.top = event.clientY + 'px';
        menu.dataset.code = event.currentTarget.dataset.code;
    },
    close: function () {
      var menu = document.getElementById('contextMenu');
      menu.style.display = 'none';
    },
    create: function () {
        var trocarCor = function (event) {
            var taskCode = $(event.target).closest('#contextMenu').attr('data-code');
            $('.list-item[data-code="' + taskCode + '"]').css('background-color', $(event.target).attr('data-color'));
        };
        var reverterCor = function(event) {
            var taskCode = $(event.target).closest('#contextMenu').attr('data-code');
            var color = $('.list-item[data-code="' + taskCode + '"]').attr('data-color');
            $('.list-item[data-code="' + taskCode + '"]').css('background-color', color);
        };
        //Criando menu
        var menu = document.createElement('li');
        menu.id = 'contextMenu';
        menu.style.position = 'absolute';
        menu.style.zIndex = '10000';

        //Criando container das cores
        var colors = document.createElement('li');
        colors.setAttribute('class', 'colors');

        //Criando bolinhas com as cores
        for (var i = 0; i < ContextMenu.colors.length; i++) {
            var color = document.createElement('div');
            color.addEventListener('mouseover', (event) => trocarCor(event));
            color.addEventListener('mouseout', (event) => reverterCor(event));
            color.addEventListener('click', (event) => {
                var taskCode = $(event.target).closest('#contextMenu').attr('data-code');
                ContextMenu.close();
                Kanban.updateTaskColor(taskCode, event.target.dataset.color);
            });
            color.setAttribute('class', 'color');
            color.setAttribute('data-color', ContextMenu.colors[i]);
            color.style.backgroundColor = ContextMenu.colors[i];
            colors.append(color);
        }

        //Colocando container das cores dentro do menu
        menu.appendChild(colors);

        //Criando HR para fazer separação
        menu.appendChild(document.createElement('hr'));

        //Criando opção editar
        var editar = document.createElement('li');
        var icoEditar = document.createElement('i');
        icoEditar.setAttribute('class', 'fa fa-edit');
        editar.appendChild(icoEditar);
        var optEditar = document.createElement('a');
        optEditar.addEventListener("click", function (event) {
            var taskCode = $(event.target).closest('#contextMenu').attr('data-code');
            TaskEditor.open(taskCode);
            ContextMenu.close();
        }, false);

        optEditar.id = 'editar';
        optEditar.href = '#';
        optEditar.textContent = 'Editar';
        editar.appendChild(optEditar);
        menu.appendChild(editar);

        //Criando opção de marcação de tempo
        var relogio = document.createElement('li');
        var icoRelogio = document.createElement('i');
        icoRelogio.setAttribute('class', 'fa fa-clock-o');
        relogio.appendChild(icoRelogio);
        var optRelogio = document.createElement('a');
        optRelogio.addEventListener("click", function (event) {
            //Colocar gatilho para marcação de tempo e desativar demais marcações
        }, false);

        optRelogio.id = 'clock';
        optRelogio.href = '#';
        optRelogio.textContent = 'Iniciar Timer';
        relogio.appendChild(optRelogio);
        menu.appendChild(relogio);

        //Criando opção de arquivar
        var arquivar = document.createElement('li');
        var icoArquivar = document.createElement('i');
        icoArquivar.setAttribute('class', 'fa fa-archive');
        arquivar.appendChild(icoArquivar);
        var optArquivar = document.createElement('a');
        //Evento clique para arquivar tarefa
        optArquivar.addEventListener("click", function (event) {
            var taskCode = $(event.target).closest('#contextMenu').attr('data-code');
            //Passa o x para filedTask
            Kanban.filedTask($('.list-item[data-code="' + taskCode + '"] i'));
            ContextMenu.close();
        }, false);

        optArquivar.id = 'arquivar';
        optArquivar.href = '#';
        optArquivar.textContent = 'Arquivar';
        arquivar.appendChild(optArquivar);
        menu.appendChild(arquivar);

        //Alterando display do menu para none
        menu.style.display = 'none';

        //Colocando o menu dentro do body
        document.body.appendChild(menu);
    }
};