var BaseReport = {
  validator: null,
  getFilters: function() {
      let filters = {};
      $('.filter').each(function(idx, filter) {
          let name = $(filter).attr('name');
          filters[name] = $(filter).val();
      });

      return filters;
  },
  generateSpreadsheet: function(button) {
      let filters = BaseReport.getFilters();
      let errors = BaseReport.validate(filters);
      if (errors.length === 0) {
          let uri = $(button).attr('data-route');
          Principal.alert(
              'Relatório',
              'O download do seu relatório está sendo realizado. Por favor, aguarde..',
              'success');
          Principal.downloadXLSX(uri, filters);
          return;
      }

      Principal.alert('Error', errors.pop(), 'danger');
  },
  setValidator: function(validator) {
      BaseReport.validator = validator;
  },
  validate: function(filters) {
      if (BaseReport.validator) {
          return BaseReport.validator(filters);
      }

      return ['Invalid validator. Contact support.'];
  }
};