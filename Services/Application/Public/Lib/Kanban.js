const ERROR_UPDATE_TASK = 'A failure occurred while trying to update the task';
const TASK_NOT_EXISTS = 'Task not exists!';
const BACKGROUND_SORTABLE = 'red';
var Kanban = {
    showInputKanban: function () {
        $.confirm({
            title: 'Enter the name of the new kanban:',
            content: '' +
            '<form action="" class="formName">' +
            '<div class="form-group">' +
            '<input type="text" placeholder="Kanban name" class="name form-control" required />' +
            '</div>' +
            '</form>',
            buttons: {
                formSubmit: {
                    text: 'Save',
                    btnClass: 'btn-success',
                    action: function () {
                        var name = this.$content.find('.name').val();
                        if(!name){
                            $.alert('Please provide a valid name!');
                            return false;
                        }
                        Kanban.new(name, function (err, data) {
                            if (err) {
                                Principal.alert('Error', 'Kanban was not created!', 'danger');
                            } else {
                                Principal.alert('Success', 'Kanban was created!', 'success', () => location.reload());
                            }
                        });
                    }
                },
                cancel: (() => {})
            },
            onContentReady: function () {
                // bind to events
                var jc = this;
                this.$content.find('form').on('submit', function (e) {
                    // if the user submits the form by pressing enter in the field.
                    e.preventDefault();
                    jc.$$formSubmit.trigger('click'); // reference the button and click it
                });
            }
        });
    },
    new: function (kanbanName, callback = false) {
        //Primeiro parâmetro de callback é o erro e o segundo são os dados
        Principal.newRequest(
            "/kanbans/register", 'POST', 'JSON', {name: kanbanName},
            {
                200: function (resp) {
                    if (callback)
                        callback(false, resp);
                },
                304: function (resp) {
                    if (callback)
                        callback(true, false);
                }
            }
        );
    },
    getTask: function (taskCode, callback) {
        Principal.newRequest(
            "/tasks/" + taskCode, 'GET', 'JSON', false,
            {
                200: function (resp) {
                    callback(resp);
                }
            }
        );
    },
    editTask: function (taskCode, name, description, taskList, callbacks) {
        let params = {
            taskCode: taskCode,
            name: name,
            description: description,
            list: taskList
        }
        let promisse =  Principal.newRequest(
            "/tasks/edit",
            'POST',
            'JSON',
            params,
            callbacks);

        return promisse;
    },
    filedTask: function (e) {
        let countingTime = $(e.closest('.list-item')).find('.item-clock')[0].classList.contains('clock-active');
        let filters = {
            idTask: $(e).closest('.list-item').attr('data-code'),
            filed: '1'
        };
        let callbacks = {
            304: function (resp) {
                Principal.alert('Error', ERROR_UPDATE_TASK, 'danger');
            },
            404: function (resp) {
                Principal.alert('Error', TASK_NOT_EXISTS, 'danger');
            },
            200: function (resp) {
                countingTime && TimerList.stop();
                $(e).parent().remove();
                Principal.alert(resp.title, resp.content, resp.type);
            }
        };
        Principal.newRequest(
            '/tasks/archiveTask',
            'POST', 
            'JSON', 
            filters, 
            callbacks);
    },
    updateTaskList: function (idTask, idList, prevTaskPosition) {
        let filters = {
            idTask: idTask,
            idList: idList,
            prevTaskPosition: prevTaskPosition
        };
        let callbacks = {
            200: function (resp) {
                for (idTask in resp.tasksPosition) {
                    var $lista = $('.list-item[data-code="' + idTask + '"]');
                    $lista.attr('data-position', resp.tasksPosition[idTask]);
                }
            },
            304: function (resp) {
                Principal.alert('Error', ERROR_UPDATE_TASK, 'danger');
            },
            404: function (resp) {
                Principal.alert('Error', TASK_NOT_EXISTS, 'danger');
            }
        };
        Principal.newRequest(
            '/tasks/updateTaskList',
            'POST', 
            'JSON', 
            filters, 
            callbacks);
    },
    updateTaskColor: function (idTask, color) {
        var oldColor = $('.list-item[data-code="' + idTask + '"]').attr('data-color');
        $('.list-item[data-code="' + idTask + '"]').css('background-color', color);
        $('.list-item[data-code="' + idTask + '"]').attr('data-color', color);

        let filters = {
            idTask: idTask,
            color: color
        };
        let callbacks = {
            304: function (resp) {
                Principal.alert('Error', ERROR_UPDATE_TASK, 'danger');
                $('.list-item[data-code="' + idTask + '"]').css('background-color', oldColor);
                $('.list-item[data-code="' + idTask + '"]').attr('data-color', oldColor);
            },
            404: function (resp) {
                Principal.alert('Error', TASK_NOT_EXISTS, 'danger');
                $('.list-item[data-code="' + idTask + '"]').css('background-color', oldColor);
                $('.list-item[data-code="' + idTask + '"]').attr('data-color', oldColor);
            }
        };

        Principal.newRequest('/tasks/updateTaskColor', 'POST', 'JSON', filters, callbacks)
                 .fail((resp) => {
                    if (resp.responseText) {
                        Principal.alert('Error', resp.responseText, 'danger');
                        $('.list-item[data-code="' + idTask + '"]').css('background-color', oldColor);
                        $('.list-item[data-code="' + idTask + '"]').attr('data-color', oldColor);
                    }
                 });
    },
    init: function () {
        Kanban.load.sortable();
        Kanban.load.scroll();

        //Criando contextMenu
        ContextMenu.create();
        
        //Criando TaskEditor
        TaskEditor.create();

        //Popover
        $('[data-toggle="popover"]').popover({
            trigger: "hover",
            placement: "bottom",
            container: "html",
            modifiers: {
                offset: { offset: '100%r80px' }
            }
        });
    }
};

Kanban.load = {
    sortable: function () {
        $('.to-do').sortable({
            connectWith: [
                '.in-progress',
                '.done'
            ],
            cursor: 'move',
            start: function (event, ui) {
                ui.item.css('background', BACKGROUND_SORTABLE);
                ui.item.css('transform', 'rotate(10deg)');
            },
            stop: function (event, ui) {
                ui.item.css('background', $(ui.item).attr('data-color'));
                ui.item.css('transform', 'rotate(0deg)');
                let idList = ui.item.parent().attr('data-code');
                let idTask = ui.item.attr('data-code');
                let prevTaskPosition = 0; //SE 0, FICA POR ÚLTIMO
                if (ui.item.next().attr('data-position'))
                    prevTaskPosition = ui.item.next().attr('data-position');
        
                Kanban.updateTaskList(idTask, idList, prevTaskPosition);
            }
        });
        $('.in-progress').sortable({
            connectWith: [
                '.to-do',
                '.done'
            ],
            cursor: 'move',
            //event: objeto de evento | ui: para resgatar o item
            start: function (event, ui) {
                ui.item.css('background', BACKGROUND_SORTABLE);
                ui.item.css('transform', 'rotate(10deg)');
            },
            stop: function (event, ui) {
                ui.item.css('background', $(ui.item).attr('data-color'));
                ui.item.css('transform', 'rotate(0deg)');
        
                let idList = ui.item.parent().attr('data-code');
                let idTask = ui.item.attr('data-code');
                let prevTaskPosition = 0; //SE 0, FICA POR ÚLTIMO
                if (ui.item.next().attr('data-position'))
                    prevTaskPosition = ui.item.next().attr('data-position');
        
                Kanban.updateTaskList(idTask, idList, prevTaskPosition);
            }
        });
        $('.done').sortable({
            connectWith: [
                '.to-do',
                '.in-progress'
            ],
            cursor: 'move',
            //event: objeto de evento | ui: para resgatar o item
            start: function (event, ui) {
                ui.item.css('background', BACKGROUND_SORTABLE);
                ui.item.css('transform', 'rotate(10deg)');
            },
            stop: function (event, ui) {
                ui.item.css('background', $(ui.item).attr('data-color'));
                ui.item.css('transform', 'rotate(0deg)');
        
                let idList = ui.item.parent().attr('data-code');
                let idTask = ui.item.attr('data-code');
                let prevTaskPosition = 0; //SE 0, FICA POR ÚLTIMO
                if (ui.item.next().attr('data-position'))
                    prevTaskPosition = ui.item.next().attr('data-position');
        
                Kanban.updateTaskList(idTask, idList, prevTaskPosition);
            }
        });
    },
    scroll: function () {
        //Cria o scroll para o kanban default
        let selector = '#' + $('.list-items').first().attr('id');
        Principal.scroll(selector);

        //Evento disparado quando aba clicada termina de ser carregada
        $('a[data-toggle="tab"]').on('hidden.bs.tab', function (e) {
            let kanbanAnt  = $(e.target).attr('href');
            let kanbanProx = $(e.relatedTarget).attr('href');
            $(kanbanAnt).find('.list-items').each(function () {
                let selector = '#' + $(this).attr('id');
                $(selector).getNiceScroll().remove();
            });
            $(kanbanProx).find('.list-items').each(function () {
                let selector = '#' + $(this).attr('id');
                Principal.scroll(selector);
            });
        });
    }
};