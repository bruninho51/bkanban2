const TIMER_SAVE_ERROR = "Um erro ocorreu ao salvar o cronômetro! Tente novamente.";
const TIMER_FORMAT_ERROR = "Digite um horário válido!";
var TimerList = {
    currentStopwatch: {
        initialized: false,
        entityInfo: '',
        start: '',
        stop: '',
        foreignKey: '',
        entity: '',
        timer: null
    },
    showModal: function () {
        $('#modalTimerList').modal('show');
    },
    save: function () {
        if (TimerList.currentStopwatch.initialized) {
            clearInterval(TimerList.currentStopwatch.timer);
            let data = {
                start: TimerList.currentStopwatch.start.format("YYYY-MM-DD HH:mm:ss"),
                stop: TimerList.currentStopwatch.stop.format("YYYY-MM-DD HH:mm:ss"),
                foreignKey: TimerList.currentStopwatch.foreignKey,
                entity: TimerList.currentStopwatch.entity,
            };
            return Principal.newRequest('/timers/', 'POST', 'JSON', data);
        }

        return new Promise((resolve, reject) => resolve(true));
    },
    start: function (entityId, entity, info) {
        promisseSave = TimerList.save()
        promisseSave.then(
            (resp) => {
                TimerList.currentStopwatch.start = moment();
                TimerList.currentStopwatch.stop = TimerList.currentStopwatch.start;
                TimerList.currentStopwatch.foreignKey = entityId;
                TimerList.currentStopwatch.entity = entity;
                TimerList.currentStopwatch.entityInfo = info;
                TimerList.setTimerDisplayInfo(
                    TimerList.currentStopwatch.start,
                    TimerList.currentStopwatch.stop,
                    TimerList.currentStopwatch.entityInfo);
                TimerList.currentStopwatch.timer = TimerList.createSetIntervalTimer();
                TimerList.currentStopwatch.initialized = true;
                document.getElementById('btnStopTimer').disabled = false;
                document.getElementById('time').blur();
                document.getElementById('time').disabled = false;
                TimerList.removeAllClockActiveClass();
                let el = $("li[data-code='" + entityId + "']").find('.item-clock');
                $(el).addClass('clock-active');
            },
            (resp) => {
                let errorMessage = TIMER_SAVE_ERROR + ': ' + resp.content;
                Principal.alert(resp.title, errorMessage, resp.type);
            }
        );
    },
    createSetIntervalTimer: function () {
        return setInterval(() => {
            if (!TimerList.currentStopwatch.paused){
                TimerList.currentStopwatch.stop = moment();
                localStorage.setItem('currentStopwatch', JSON.stringify(TimerList.currentStopwatch));
                TimerList.setTimerDisplayInfo(
                    TimerList.currentStopwatch.start,
                    TimerList.currentStopwatch.stop
                );
            }
        }, 1000);
    },
    resumeCurrentStopwatch: function () {
        let strTime = document.getElementById('time').value;
        let time = moment.duration(strTime);
        let pattern = RegExp(/(?:[01]\d|2[0-3]):(?:[0-5]\d):(?:[0-5]\d)/);
        if (pattern.test(strTime)) {
            TimerList.currentStopwatch.start = moment().subtract(time);
            TimerList.currentStopwatch.paused = false;
        } else {
            throw Error(TIMER_FORMAT_ERROR);
        }
    },
    removeAllClockActiveClass: function () {
        $('.clock-active').removeClass('clock-active');
    },
    stop: function () {
        promisseSave = TimerList.save();
        promisseSave.then(
            () => {
                TimerList.currentStopwatch.start = '';
                TimerList.currentStopwatch.foreignKey = '';
                TimerList.currentStopwatch.entity = '';
                TimerList.currentStopwatch.timer = null;
                TimerList.currentStopwatch.initialized = false;
                TimerList.currentStopwatch.entityInfo = '';
                document.getElementById('btnStopTimer').disabled = true;
                document.getElementById('time').blur();
                document.getElementById('time').disabled = true;
                TimerList.stopTimerDisplayInfo('statusTimer');
                TimerList.removeAllClockActiveClass();
                localStorage.removeItem('currentStopwatch');
                TimerList.clearTimerDisplayInfo();
            },
            () => {
                Principal.alert("Erro", TIMER_SAVE_ERROR, "danger");
            }
        );
    },
    setTimerDisplayInfo: function (start, stop, info = false) {
        if (info) {
            document.getElementById('statusTimer').innerText = info;
        }
        let totalSeconds = parseInt(stop.diff(start, 'seconds'));
        let hours = Math.floor(totalSeconds / 3600);
        totalSeconds -= 3600 * hours;
        let minutes = Math.floor(totalSeconds / 60);
        totalSeconds -= 60 * minutes;
        let seconds = totalSeconds;
        let strTime = ("00" + hours).slice(-2) + ':' + ("00" + minutes).slice(-2) + ':' + ("00" + seconds).slice(-2);
        let inputTime  = document.getElementById('time');
        inputTime.value  = strTime;
    },
    clearTimerDisplayInfo: function () {
        let inputTime  = document.getElementById('time');
        inputTime.value  = "";
    },
    stopTimerDisplayInfo: function (elementId) {
        document.getElementById('statusTimer').innerText = '';
        document.getElementById('timeDisplay').innerText = '';
    },
    edit: function () {
        TimerList.currentStopwatch.paused = true;
    },
    init: function () {
        let timeInput = document.getElementById('time');
        $(time).mask('00:00:00');

        let currentStopwatch = localStorage.getItem('currentStopwatch');

        if (currentStopwatch !== null) {
            currentStopwatch = JSON.parse(currentStopwatch);
            currentStopwatch.start = moment(currentStopwatch.start);
            currentStopwatch.stop = moment(currentStopwatch.stop);
            TimerList.currentStopwatch = currentStopwatch;
            TimerList.setTimerDisplayInfo(
                TimerList.currentStopwatch.start,
                TimerList.currentStopwatch.stop,
                TimerList.currentStopwatch.entityInfo);
            TimerList.currentStopwatch.timer = TimerList.createSetIntervalTimer();
            document.getElementById('btnStopTimer').disabled = false;
            timeInput.disabled = false;
            let el = $("li[data-code='" + TimerList.currentStopwatch.foreignKey + "']").find('.item-clock');
            $(el).addClass('clock-active');
        }
        timeInput.addEventListener('click', TimerList.edit);
        timeInput.addEventListener('blur', function () {
            try {
                TimerList.resumeCurrentStopwatch();
            } catch (error) {
                Principal.alert('Erro no Temporizador', error.message, 'warning');
            }
        });
    }
};