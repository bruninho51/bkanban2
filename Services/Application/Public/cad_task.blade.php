@section('content')
    <form action="/tasks/register" method="post" class="container-form">
        <h2>Register Task</h2>
        <div class="form-group">
            <label for="name">Name: </label>
            <input type="text" name="name" class="form-control">
        </div>
        <div class="form-group">
            <label for="description">Description: </label>
            <textarea name="description" class="form-control"></textarea>
        </div>
        <div class="form-group">
            <label for="list">List: </label>
            <select name="list" class="form-control">
                <option value="1">TO DO</option>
                <option value="2">Work in Progress</option>
                <option value="3">DONE</option>
            </select>
        </div>
        <div class="form-group">
            <label for="kanbans">Kanban: </label>
            @if(isset($kanbans) && count($kanbans) > 0)
                <select name="kanban" class="form-control">
                    @foreach ($kanbans as $kanban)
                        <option value="{{$kanban->id}}">{{$kanban->name}}</option>
                    @endforeach
                </select>
            @endif
        </div>
        <div class="form-group sr-only">
            <label for="users_email">User: </label>
            <select name="users_email" class="form-control">
                <option value="{{$user->email}}">{{$user->email}}</option>
            </select>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </form>
@endsection

@include('navbar')
@include('templates.template')