<div class="row container-lists">
    <div class="col-md-4">
        <ul class="list">
            <h1>TO DO</h1>
            <div class="list-items to-do" data-code="1" id="{{$kanban->getIdentity()}}_to-do">
                @include('templates.listItems', ['tasks' => $kanban->tasks[1]])
            </div>
        </ul>
    </div>

    <div class="col-md-4">
        <ul class="list">
            <h1>IN PROGRESS</h1>
            <div class="list-items in-progress" data-code="2" id="{{$kanban->getIdentity()}}_in-progress">
                @include('templates.listItems', ['tasks' => $kanban->tasks[2]])
            </div>
        </ul>
    </div>

    <div class="col-md-4">
        <ul class="list">
            <h1>DONE</h1>
            <div class="list-items done" data-code="3" id="{{$kanban->getIdentity()}}_done">
                @include('templates.listItems', ['tasks' => $kanban->tasks[3]])
            </div>
        </ul>
    </div>
</div>