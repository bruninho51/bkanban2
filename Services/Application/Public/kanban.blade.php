@section('kanban')
<ul class="nav nav-tabs" id="myTab" role="tablist">
    @foreach($kanbans as $kanban)
        @if ($loop->first)
            <li class="nav-item">
                <a class="nav-link active" id="{{$kanban->getIdentity()}}-tab" data-toggle="tab" href="#{{$kanban->getIdentity()}}" role="tab" aria-controls="{{$kanban->getIdentity()}}" aria-selected="true">{{$kanban->name}}</a>
            </li>
        @else
            <li class="nav-item">
                <a class="nav-link" id="{{$kanban->getIdentity()}}-tab" data-toggle="tab" href="#{{$kanban->getIdentity()}}" role="tab" aria-controls="{{$kanban->getIdentity()}}" aria-selected="false">{{$kanban->name}}</a>
            </li>
        @endif
    @endforeach
</ul>
<div class="tab-content" id="myTabContent">
    @foreach ($kanbans as $kanban)
        @if ($loop->first)
            <div class="tab-pane fade show active" data-idKanban="{{$kanban->id}}" id="{{$kanban->getIdentity()}}" role="tabpanel" aria-labelledby="{{$kanban->getIdentity()}}-tab">
                @include('tableau', ['kanban' => $kanban])
            </div>
        @else
            <div class="tab-pane fade" data-idKanban="{{$kanban->id}}" id="{{$kanban->getIdentity()}}" role="tabpanel" aria-labelledby="{{$kanban->getIdentity()}}-tab">
                @include('tableau', ['kanban' => $kanban])
            </div>
        @endif
    @endforeach
</div>
@endsection