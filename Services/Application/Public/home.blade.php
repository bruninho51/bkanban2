@include('kanban')
@section('content')
    <div class="container">
        @yield('kanban')
    </div>    
@endsection

@include('bar_utils')
@include('navbar')
@include('timer_list')
@include('templates.template')