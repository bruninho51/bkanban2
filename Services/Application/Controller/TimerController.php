<?php


namespace Services\Application\Controller;

use Exception;
use Services\Infraestructure\Persistence\TimerMapper;
use Services\Domain\Timer;

class TimerController extends Controller
{
    public function save($request, $response, $args)
    {
        $data = (object) $request->getParsedBody();

        $startIsset              = isset($data->start) && $data->start;
        $stopIsset               = isset($data->stop) && $data->stop;
        $foreignKeyIsset         = isset($data->foreignKey) && $data->foreignKey;
        $entityIsset             = isset($data->entity) && $data->entity;

        if ($startIsset && $stopIsset && $foreignKeyIsset && $entityIsset) {
            $timer = new Timer();
            try {
                $timer->start = strtotime($data->start);
                $timer->stop  = strtotime($data->stop);
                if (isset($data->id) && $data->id) {
                    $timer->id = $data->id;
                }
                $timer->foreign_key = $data->foreignKey;
                $timer->entity = $data->entity;
                $timer = TimerMapper::save($timer);
                return $response->withJson([
                    'title' =>  'Success',
                    'type' =>   'success',
                    'content' => Timer::MSG_CAD_SUCCESS,
                    'task' => $timer->toArray()
                ],200);
            } catch (Exception $ex) {
                return $response->withJson([
                    'title' =>  'Error',
                    'type' =>   'danger',
                    'content' => $ex->getMessage()
                ],500);
            }
        }

        return $response->withJson([
            'title' =>  'Error',
            'type' =>   'danger',
            'content' => Timer::INVALID_INFORMATION
        ],500);
    }
}