<?php


namespace Services\Application\Controller;

use Services\Application\Spreadsheets\SpreadsheetXLSXDownload;
use Services\Application\Spreadsheets\TimeReport;
use Services\Infraestructure\Persistence\TaskMapper;
use Services\Infraestructure\Persistence\TimerMapper;
use Slim\Http\Request;
use Slim\Http\Response;
use DateTIme;
use ArrayObject;
use Services\Application\Spreadsheets\TimeReport as SpreadsheetTimeReport;

class TimerReportController extends ReportController
{

    use SpreadsheetXLSXDownload;

    public function index(Request $request, Response $response, $args)
    {
        $this->view->render($response, 'base_report', [
            'title' => 'Service Time Report',
            'stylesheets' => ['base_report', 'filters'],
            'scripts' => ['BaseReport/BaseReport', 'TimerReport'],
            'filters' => $this->getFilters(),
            'messages' => $this->flash->getMessages()
        ]);
    }

    public function generateSpreadsheet(Request $request, Response $response, $args)
    {
        $arguments = (object) $request->getParsedBody();
        $issetStart = $arguments->start;
        $issetStop  = $arguments->stop;

        if ($issetStart && $issetStop) {
            $name = date('Ymd') . 'spreadsheet.xlsx';

            $where = [];
            $where[] = [
                'key' => 'start',
                'operator' => 'between',
                'value' => [
                    new DateTime("{$arguments->start} 00:00:00"),
                    new DateTime("{$arguments->stop} 23:59:00")
                ]
            ];

            $data = TimerMapper::findByUser($_SESSION['user'], $where);
            $timeReport = new SpreadsheetTimeReport(new ArrayObject($data));
            $spreadsheet = $timeReport->generate();
            $this->downloadSpreadsheet($spreadsheet, $name);
            return;
        }

        $this->flash->addMessage('Period not reported.');
        return $response->withRedirect($this->router->pathFor('time.serviceTimeReport'), 200);
    }

    private function getFilters()
    {
        $filters = new \Services\Application\Filters\Filter();
        $filters->setButtonName('Filtrar');
        $filters->enableButtonFilter = false;

        $datePeriod = new \Services\Application\Filters\DatePeriod();
        $datePeriod->addClass('filter');
        $datePeriod->setIdStartPeriod('start');
        $datePeriod->setNameStartPeriod('start');
        $datePeriod->setIdStopPeriod('stop');
        $datePeriod->setNameStopPeriod('stop');

        $buttonGenSpreadsheet = new \Services\Application\Filters\ActionButton('Gerar Excel');
        $buttonGenSpreadsheet->setRoute($this->router->pathFor('timeReports.generateSpreadsheet'));
        $buttonGenSpreadsheet->setScript(self::DEFAULT_GEN_SPREADSHEET);
        $buttonGenSpreadsheet->setId('btnGerarExcel');
        $buttonGenSpreadsheet->setName('btnGerarExcel');

        $filters->add($datePeriod);
        $filters->addButton($buttonGenSpreadsheet);

        return $filters;
    }
}