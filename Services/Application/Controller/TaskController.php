<?php

namespace Services\Application\Controller;

use Services\Application\Config\Configuration;
use Exception;
use Slim\Http\Request;
use Slim\Http\Response;
use Services\Domain\Task;
use Service\Domain\Kanban;
use Services\Infraestructure\Persistence\Mapper;
use Services\Infraestructure\Persistence\TaskMapper;
use Services\Infraestructure\Persistence\UserMapper;
use Services\Infraestructure\Persistence\KanbanMapper;

class TaskController extends Controller
{
   public function index(Request $request, Response $response, $args)
   {
       $userEmail = $_SESSION['user']->email;
        $this->view->render($response, 'cad_task', [
            'title' => 'Cadastro de Tarefa',
            'stylesheets' => ['cad_task'],
            'messages' => $this->flash->getMessages(),
            'user' => $_SESSION['user'],
            'kanbans' => KanbanMapper::findByWhere([['key' => 'users_email', 'value' => $userEmail]])
        ]);
   }
    
    public function toList(Request $request, Response $response, $args)
    {
        echo 'Listagem';
        return $response;
    }

    public function getTask(Request $request, Response $response, $args)
    {
        $task = TaskMapper::findById($args['taskCode']);
        return $response->withJson($task->toArray(), 200);
    }
   
   public function register(Request $request, Response $response, $args)
   {
       $data = (object) $request->getParsedBody();

       $isName = isset($data->name) && $data->name;
       $isDescription = isset($data->description) && $data->description;
       $isList = isset($data->list) && $data->list;

       if ($isName && $isDescription && $isList) {

           if (isset($data->id)) {
               $task = TaskMapper::findById($data->id);
           } else {
               $task = new Task();
           }

           $task->name = $data->name;
           $task->description = $data->description;
           $task->list = $data->list;
           $task->users_email = $data->users_email ?? $task->users_email;
           $task->filed = '0';
           $task->kanban = KanbanMapper::findById($data->kanban);

           try {
               TaskMapper::save($task);
           } catch (Exception $ex) {
               $this->flash->addMessage('alertDanger', $ex->getMessage() . '.If the problem persists, contact your administrator.');
               return $response->withRedirect($this->router->pathFor('tasks.register'), 200);
           }

           $this->flash->addMessage('alertSuccess', Task::MSG_CAD_SUCCESS, 200);
           return $response->withRedirect($this->router->pathFor('tasks.register'));
       }
       $this->flash->addMessage('alertDanger', Task::MSG_DATA_MISING);
       return $response->withRedirect($this->router->pathFor('tasks.register'), 200);
   }

   public function edit(Request $request, Response $response, $args)
   {
       $data = (object) $request->getParsedBody();

       $isName = isset($data->name) && $data->name;
       $isDescription = isset($data->description) && $data->description;
       $isList = isset($data->list) && $data->list;
       $isId = isset($data->taskCode) && $data->taskCode;

       if ($isName && $isDescription && $isList && $isId) {

           $task = TaskMapper::findById($data->taskCode);

           $task->name = $data->name;
           $task->description = $data->description;
           $task->list = $data->list;
           $task->users_email = $data->users_email ?? $task->users_email;
           $task->filed = '0';

           try {
               TaskMapper::save($task);
           } catch (Exception $ex) {
               $this->flash->addMessage('alertDanger', $ex->getMessage() . '.If the problem persists, contact your administrator.');
           }

           return $response->withJson([
               'title' =>  'Success',
               'type' =>   'success',
               'content' => Task::MSG_EDIT_SUCCESS,
               'task' => $task->toArray()
           ],200);
       }

       return $response->withJson([
           'title' =>  'Error',
           'type' =>   'danger',
           'content' => Task::MSG_DATA_MISING
       ],304);
   }

   public function updateTaskList(Request $request, Response $response, $args)
   {
        $data = (object) $request->getParsedBody();
        $isTaskId   = isset($data->idTask) && $data->idTask;
        $isTaskList = isset($data->idList) && $data->idList;
        $isPrevTaskPosition = isset($data->prevTaskPosition) && is_numeric($data->prevTaskPosition);
        $user = $_SESSION['user']; //Validar se a tarefa é do usuário logado

        if ($isTaskId && $isTaskList && $isPrevTaskPosition) {
            try {
                $task = TaskMapper::findById($data->idTask);
                $task->list = $data->idList;
                $task->position = $data->prevTaskPosition;

                TaskMapper::save($task);

                return $response->withJson([
                    'title' =>  'Success',
                    'type' =>   'success',
                    'content' => Task::MSG_CAD_SUCCESS,
                    'tasksPosition' => TaskMapper::getTasksPositionByList($data->idList)
                ],200);
            } catch (Exception $ex) {
                if ($ex->getMessage() === TaskMapper::MODEL_NOT_SAVED) {
                    return $response->withStatus(304);
                } elseif ($ex->getMessage() === TaskMapper::MODEL_GET_FAILURE) {
                    return $response->withStatus(404);
                } else {
                    return $response->withJson([
                        'title' => 'Error',
                        'type' => 'danger',
                        'content' => $ex->getMessage()
                    ], 200);
                }
            }
        }
   }

   public function archiveTask(Request $request, Response $response, $args)
   {
       $data = (object) $request->getParsedBody();
       $validTaskId   = isset($data->idTask) && $data->idTask;
       $validFiledTask = isset($data->filed) && is_numeric($data->filed);
       $user = $_SESSION['user']; //Validar se a tarefa é do usuário que está logado

       if ($validTaskId && $validFiledTask) {
           try {
               $task = TaskMapper::findById($data->idTask);
               $task->filed = $data->filed;

               TaskMapper::save($task);

               return $response->withJson([
                   'title' =>  'Success',
                   'type' =>   'success',
                   'content' => Task::MSG_TASK_FILED
               ],200);
           } catch (Exception $ex) {
               if ($ex->getMessage() === TaskMapper::MODEL_NOT_SAVED) {
                   return $response->withStatus(304);
               } elseif ($ex->getMessage() === TaskMapper::MODEL_GET_FAILURE) {
                   return $response->withStatus(404);
               } else {
                   return $response->withJson([
                       'title' => 'Error',
                       'type' => 'danger',
                       'content' => $ex->getMessage()
                   ], 200);
               }
           }
       }
   }

   public function updateTaskColor(Request $request, Response $response, $args)
   {
        $data = (object) $request->getParsedBody();
        $validTaskId = isset($data->idTask) && $data->idTask;
        $validColor  = isset($data->color) && $data->color;

        if ($validTaskId && $validColor) {
            try {
                $task = TaskMapper::findById($data->idTask);
                $task->color = $data->color;

                TaskMapper::save($task);

                return $response->withJson([
                    'title' => 'Success',
                    'type'  => 'success',
                    'content' => Task::MSG_CAD_SUCCESS
                ]);
            } catch (Exception $ex) {
                if ($ex->getMessage() === TaskMapper::MODEL_NOT_SAVED) {
                    return $response->withStatus(304);
                } elseif ($ex->getMessage() === TaskMapper::MODEL_GET_FAILURE) {
                    return $response->withStatus(404);
                } else {
                    return $response->withJson([
                        'title' => 'Error',
                        'type' => 'danger',
                        'content' => $ex->getMessage()
                    ], 200);
                }
            }
        }
   }
}