<?php

namespace Services\Application\Controller;

use Services\Infraestructure\Persistence\KanbanMapper;
use Services\Domain\Kanban;
use Exception;

class KanbanController extends Controller
{

   public function register($request, $response, $args)
   {
      $data = (object) $request->getParsedBody();
      $isName = isset($data->name) && $data->name;
      if ($isName) {
        $kanban = new Kanban();
        $kanban->name = $data->name;
        $kanban->users_email = $_SESSION['user']->email;
        try {
            KanbanMapper::save($kanban);
        } catch (Exception $ex) {
            return $response->withJson([
                'title' =>  'Error',
                'type' =>   'danger',
                'content' => Kanban::MSG_DATA_MISING
            ],304);
        }

        return $response->withJson([
            'title' =>  'Success',
            'type' =>   'success',
            'content' => Kanban::MSG_CAD_SUCCESS,
            'kanban' => $kanban->toArray()
        ],200);
      }

      return $response->withJson([
        'title' =>  'Error',
        'type' =>   'danger',
        'content' => Kanban::MSG_DATA_MISING
      ],304);
   }
}