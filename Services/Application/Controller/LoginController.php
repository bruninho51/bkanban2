<?php

namespace Services\Application\Controller;

use Services\Domain\User;
use Services\Application\Authenticate;

class LoginController extends Controller
{

   public function index($request, $response, $args)
   {
       if (isset($_SESSION['user'])) {
           return $response->withRedirect($this->router->pathFor('home'), 200);
       }
       
      return $this->view->render($response, 'login', [
         'title' => 'Entre no Kanban',
         'stylesheets' => ['login'],
         'messages' => $this->flash->getMessages()
      ]);
   }
   
   public function login($request, $response, $args)
   {   
       $post = $request->getParams();
       
       $user = new User;
       $user->email = $post['email'];
       $user->password = $post['password'];
       
       try {
           if (Authenticate::attempt($user)) {
                //return $response->withRedirect($this->router->pathFor('home'), 200);
                //Colocado porque withRedirect não está funcionando na hospedagem
                $url = $this->router->pathFor('home');
                header("location: {$url}");
                exit;
           } 
       } catch (\Exception $ex) {
           $this->flash->addMessage('error', $ex->getMessage());
           return $response->withRedirect($this->router->pathFor('login'), 200);
       }
   }
   
   public function logoof($request, $response, $args)
   {
       session_destroy();
       //return $response->withRedirect($this->router->pathFor('login'), 200);
       //Colocado porque withRedirect não está funcionando na hospedagem 
       $url = $this->router->pathFor('login');
       header("location: {$url}");
       exit;
   }
}