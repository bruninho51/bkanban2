<?php

namespace Services\Application\Controller;

use Services\Infraestructure\Persistence\KanbanMapper;
use Services\Domain\Task;
use Services\Infraestructure\Persistence\UserMapper;

class HomeController extends Controller
{

   public function index($request, $response, $args)
   {
      $user = $_SESSION['user'];
      $kanbans = KanbanMapper::findWithTasksByUser($user);
      return $this->view->render($response, 'home', [
         'title' => 'Kanban',
         'stylesheets' => ['home'],
         'scripts' => ['Kanban'],
         'kanbans' => $kanbans
      ]);
   }
}