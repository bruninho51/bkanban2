<?php

namespace Services\Application\Controller;

use Services\Application\Config\Configuration;
use Exception;
use Slim\Http\Request;
use Slim\Http\Response;
use Services\Domain\User;
use Services\Infraestructure\Persistence\UserMapper;

class UserController extends Controller {

    public function index(Request $request, Response $response, $args)
    {
        $this->view->render($response, 'cad_user', [
            'title' => 'Cadastro de Usuário',
            'stylesheets' => ['cad_user'],
            'messages' => $this->flash->getMessages()
        ]);
    }
    
    public function toList(Request $request, Response $response, $args)
    {
        
    }
    
    public function register(Request $request, Response $response, $args)
    {
        $data = (object) $request->getParsedBody();

        $isName = isset($data->name) && $data->name;
        $isEmail = isset($data->email) && $data->email;
        $isPassword = isset($data->password) && $data->password;

        if ($isName && $isEmail && $isPassword) {
            $user = new User();
            $user->name     = $data->name;
            $user->email    = $data->email;
            $user->password = $data->password;

            try {
                UserMapper::save($user);
            } catch (Exception $ex) {
                $this->flash->addMessage('alertDanger', $ex->getMessage());
                return $response->withRedirect($this->router->pathFor('users.register'), 200);
            }

            $this->flash->addMessage('alertSuccess', User::MSG_CAD_SUCCESS);
            return $response->withRedirect($this->router->pathFor('users.register'), 200);
        }

        $this->flash->addMessage('alertDanger', User::MSG_DATA_MISING);
        return $response->withRedirect($this->router->pathFor('users.register'), 200);
    }
}
