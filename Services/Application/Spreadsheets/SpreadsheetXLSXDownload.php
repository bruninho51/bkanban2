<?php


namespace Services\Application\Spreadsheets;


use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

trait SpreadsheetXLSXDownload
{
    public function downloadSpreadsheet(Spreadsheet $spreadsheet, $name) : void
    {
        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$name.'"');
        header('Cache-Control: max-age=0');
        $writer->save('php://output');
    }
}