<?php


namespace Services\Application\Spreadsheets;


use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;

trait StyleSpreadsheet
{
    public function getStyleBorder()
    {
        return [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => array('rgb' => '000000'),
                ],
            ],
        ];
    }

    public function getStyleHeader()
    {
        $styleBorders = $this->getStyleBorder();
        return [
            'font'  => [
                'bold'  => false,
                'color' => array('rgb' => 'FFFFFF'),
                'size'  => 12,
                'name'  => 'Calibre'
            ],
            'alignment' => [
                'horizontal' => Alignment::HORIZONTAL_CENTER
            ],
            'fill' => [
                'fillType' => Fill::FILL_GRADIENT_LINEAR,
                'color' => array('rgb' => '3D3D3D')
            ],
            'borders' => $styleBorders['borders']
        ];
    }
}