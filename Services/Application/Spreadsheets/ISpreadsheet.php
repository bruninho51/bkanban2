<?php


namespace Services\Application\Spreadsheets;


use PhpOffice\PhpSpreadsheet\Spreadsheet;

interface ISpreadsheet
{
    public function generate() : Spreadsheet;
}