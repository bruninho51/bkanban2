<?php


namespace Services\Application\Spreadsheets;

use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use ArrayObject;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use DateTime;
use Services\Application\Config\Configuration;
use Services\Application\Filters\Date;
use Services\Domain\Timer;

class TimeReport
{

    use StyleSpreadsheet;

    private $data;
    private $header;
    private $fromTheLine;

    public function __construct(ArrayObject $data = null)
    {
        $this->header = [
            'Task',
            'Description',
            'Filed',
            'Start',
            'Stop',
            'Total Time'
        ];

        $this->fromTheLine = 4;
        $this->data = new ArrayObject();
        if ($data) {
            $this->data = $data;
        }
    }

    public function generate() : Spreadsheet
    {
        $spreadsheet = new Spreadsheet();
        $properties = $spreadsheet->getProperties();
        $properties->setTitle('Timers');
        $properties->setDescription('BKanban Task Timers');
        $planTimers = $spreadsheet->getActiveSheet();
        $planTimers->setTitle('Timers');
        $planTimers = $this->mountHeader($planTimers);

        //PEGA AS LETRAS DAS COLUNAS..
        $colMerge  = Coordinate::stringFromColumnIndex(1);
        $colMerge2 = Coordinate::stringFromColumnIndex(count($this->header));
        $lineMerge = $this->fromTheLine - 1;
        $planTimers->mergeCells("{$colMerge}1:{$colMerge2}{$lineMerge}"); //MESCLA AS CÉLULAS
        $planTimers = $this->addLogo($planTimers);

        $iterator = $this->data->getIterator();
        $column = 1;
        $line = $this->fromTheLine + 1;
        while ($iterator->valid()) {
            $timer = $iterator->current();
            $formattedData = $this->formatData($timer);
            foreach ($formattedData as $data) {
                $planTimers->getStyleByColumnAndRow($column, $line)->applyFromArray($this->getStyleBorder());
                $planTimers->setCellValueByColumnAndRow($column++, $line, $data);
            }

            $column = 1;
            $line++;
            $iterator->next();
        }

        foreach (range('A', $planTimers->getHighestDataColumn()) as $col) {
            $planTimers
                ->getColumnDimension($col)
                ->setAutoSize(true);
        }

        return $spreadsheet;
    }

    private function mountHeader(Worksheet $worksheet) : Worksheet
    {
        $column = 1;
        foreach ($this->header as $header) {
            $worksheet->getStyleByColumnAndRow($column, $this->fromTheLine)->applyFromArray($this->getStyleHeader());
            $worksheet->setCellValueByColumnAndRow($column++, $this->fromTheLine, $header);
        }

        return $worksheet;
    }

    private function formatData(Timer $timer)
    {
        $filed = $timer->entityModel->filed ? 'Yes' : 'No';
        $start = (new DateTime())->setTimestamp($timer->start);
        $stop = (new DateTime())->setTimestamp($timer->stop);
        $diff = $start->diff($stop);
        $diffHours   = str_pad($diff->format('%H'), 2, "0", STR_PAD_LEFT);
        $diffMinutes = str_pad($diff->format('%i'), 2, "0", STR_PAD_LEFT);
        $diffSeconds = str_pad($diff->format('%s'), 2, "0", STR_PAD_LEFT);
        $formatted = [
            $timer->entityModel->name,
            $timer->entityModel->description,
            $filed,
            $start->format('d/m/Y H:i:s'),
            $stop->format('d/m/Y H:i:s'),
            "{$diffHours}:{$diffMinutes}:{$diffSeconds}"
        ];
        return $formatted;
    }

    private function addLogo(Worksheet $worksheet) : Worksheet
    {
        $dw = new Drawing();
        $dw->setName('test_img');
        $dw->setDescription('test_img');
        $dw->setPath(Configuration::get('env', 'IMAGES_PATH') . 'icon_black.png');
        $dw->setCoordinates('A1');
        //setOffsetX works properly
        $dw->setOffsetX(10);
        $dw->setOffsetY(15);
        //set width, height
        $dw->setWidth(100);
        $dw->setHeight(35);
        $dw->setWorksheet($worksheet);

        return $worksheet;
    }
}