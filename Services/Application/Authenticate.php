<?php

namespace Services\Application;

use Services\Domain\User;
use Services\Infraestructure\Persistence\UserMapper;
use Exception;

class Authenticate {
    
    const INVALID_CREDENTIALS = 'Invalid credentials';
    
    public static function attempt(User $user) : bool
    {
        $filter = [
            0 => [
                'key' => 'email',
                'value' => $user->email
            ],
            1 => [
                'key' => 'password',
                'value' => $user->password
            ]
        ];
        
        $users = UserMapper::findByWhere($filter);
        
        if ($users) {
            session_start();
            $_SESSION['user'] = current($users);
            
            return true;
        }
        
        throw new Exception(self::INVALID_CREDENTIALS);
    }
    
}
