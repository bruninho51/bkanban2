<?php


namespace Services\Application\Filters;


class ActionButton extends \FilterCreator\ButtonAction
{
    public function __construct($title)
    {
        parent::__construct($title);
        $this->addClass('btn');
        $this->addClass('btn-primary');
    }
}