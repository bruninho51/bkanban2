<?php


namespace Services\Application\Filters;

use FilterCreator\FilterInput;
use \FilterCreator\Contracts\IFilter;
use Illuminate\Support\Arr;

class DatePeriod extends FilterInput implements IFilter
{

    /**
     * @var \ArrayObject
     */
    private $inputs;

    const START_PERIOD = 0;
    const STOP_PERIOD = 1;

    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->label = 'Período: ';
        $this->inputs = new \ArrayObject();
        $inicio = new \Services\Application\Filters\Date();
        $inicio->setId('periodoInicio');
        $inicio->setName('periodoInicio');
        $fim = new \Services\Application\Filters\Date();
        $fim->setId('periodoFim');
        $fim->setName('periodoFim');
        $this->inputs->append($inicio);
        $this->inputs->append($fim);
    }

    public function setIdStartPeriod($id)
    {
        $this->inputs->offsetGet(self::START_PERIOD)->setId($id);
    }

    public function setNameStartPeriod($name)
    {
        $this->inputs->offsetGet(self::START_PERIOD)->setName($name);
    }

    public function setIdStopPeriod($id)
    {
        $this->inputs->offsetGet(self::STOP_PERIOD)->setId($id);
    }

    public function setNameStopPeriod($name)
    {
        $this->inputs->offsetGet(self::STOP_PERIOD)->setName($name);
    }

    public function mount(): String
    {
        $html = '';
        $label = $this->createLabel ? "<label for='{$this->name}'>{$this->label}</label>" : '';
        $html .= $label;
        $iterator = $this->inputs->getIterator();
        while ($iterator->valid()) {

            $html .= $iterator->key() === 1 ? ' Até ' : '';

            $input = $iterator->current();

            $classes = implode(' ', (array) $input->classes);
            $value = '';
            if ($input->getValues()->count() > 0) {
                $value = $input->getValues()->offsetGet(0) ?: '';
            }
            $label = $input->createLabel ? "<label for='{$input->name}'>{$input->label}</label>" : '';
            $placeholder = !$label ? $input->label : '';
            $html .= $label;
            $html .= "<input
                    type='{$input->type}' 
                    id='{$input->id}' 
                    name='{$input->name}' 
                    class='{$classes}'
                    placeholder='{$placeholder}'
                    value='{$value}'>";

            $iterator->next();
        }

        return $html;
    }

    public function addClass(String $className) : void
    {
        $it = $this->inputs->getIterator();
        while ($it->valid()) {
            $it->current()->classes[] = $className;
            $it->next();
        }
    }
}