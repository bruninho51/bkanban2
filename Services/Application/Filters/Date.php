<?php


namespace Services\Application\Filters;

use FilterCreator\FilterInput;
use \FilterCreator\Contracts\IFilter;

class Date extends FilterInput implements IFilter
{
    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->type = 'date';
        $this->createLabel = false;
    }


}