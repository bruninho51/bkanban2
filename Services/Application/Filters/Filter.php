<?php


namespace Services\Application\Filters;

use FilterCreator\Filter as FilterCreator;


class Filter extends FilterCreator
{
    protected function createButton()
    {
        $onClick = "onclick='{$this->attachment}'";
        $button = "<br><input type='button' class='btn btn-primary mr-2' {$onClick} value='{$this->buttonName}'>";

        return $button;
    }
}