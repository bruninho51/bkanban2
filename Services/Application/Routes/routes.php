<?php

require_once 'vendor/autoload.php';

use Slim\App;
use Services\Application\Middleware\Auth;

//Login
$app->redirect('/', 'login');
$app->get('/login', 'LoginController:index')->setName('login');
$app->post('/login', 'LoginController:login');
$app->get('/login/logoof', 'LoginController:logoof');

$app->group('', function (App $app) {
    //Home
    $app->get('/home', 'HomeController:index')->setName('home'); //Kanban

    //Tasks
    $app->group('/tasks', function (App $app) {
        $app->redirect('/', '/list');
        $app->get('/list', 'TaskController:toList')->setName('tasks.list'); //Listagem das tarefas do Kanban
        $app->get('/register', 'TaskController:index')->setName('tasks.register'); //Cadastra tarefas do Kanban
        $app->get('/{taskCode}', 'TaskController:getTask')->setName('tasks.getTask');
        $app->post('/edit', 'TaskController:edit')->setName('tasks.edit');
        $app->post('/register', 'TaskController:register');
        $app->post('/updateTaskList', 'TaskController:updateTaskList')->setName('tasks.updateTaskList');
        $app->post('/archiveTask', 'TaskController:archiveTask')->setName('tasks.archiveTask');
        $app->post('/updateTaskColor', 'TaskController:updateTaskColor')->setName('tasks.updateTaskColor');
    });

    //Timer
    $app->group('/timers', function (App $app) {
        $app->post('/', 'TimerController:save');
        $app->get('/statistics/serviceTimeReport', 'TimerReportController:index')
        ->setName('time.serviceTimeReport');
        $app->post('/statistics/serviceTimeReport', 'TimerReportController:generateSpreadsheet')
        ->setName('timeReports.generateSpreadsheet');
    });

    //Users
    $app->group('/users', function (App $app) {
        $app->get('/list', 'UserController:toList')->setName('users.list'); //Lista usuários
        $app->get('/register', 'UserController:index')->setName('users.register'); //Cadastra usuários
        $app->post('/register', 'UserController:register');
    });
    $app->group('/kanbans', function (App $app) {
        $app->post('/register', 'KanbanController:register');
    });
})->add(new Auth($app->getContainer()));