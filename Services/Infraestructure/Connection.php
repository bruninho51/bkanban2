<?php 

namespace Services\Infraestructure;

use PDO;
use Services\Application\Config\Configuration;

final class Connection {

    private static $con;

    private static final function getStringConnection()
    {
        $str = __DIR__ . '/Storage/' . Configuration::get('database', 'databaseArq');
        return 'sqlite:' . $str;
    }

    public static final function get()
    {
        if (!self::$con) {
            self::$con = new PDO(self::getStringConnection());
            self::$con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }

        return self::$con;
    }
    
    public static final function close()
    {
        self::$con = null;
    }
}