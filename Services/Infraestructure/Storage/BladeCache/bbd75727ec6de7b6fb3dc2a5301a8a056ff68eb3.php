<?php $__env->startSection('content'); ?>
    <form action="/tasks/register" method="post" class="container-form">
        <h2>Register Task</h2>
        <div class="form-group">
            <label for="name">Name: </label>
            <input type="text" name="name" class="form-control">
        </div>
        <div class="form-group">
            <label for="description">Description: </label>
            <textarea name="description" class="form-control"></textarea>
        </div>
        <div class="form-group">
            <label for="list">List: </label>
            <select name="list" class="form-control">
                <option value="1">TO DO</option>
                <option value="2">Work in Progress</option>
                <option value="3">DONE</option>
            </select>
        </div>
        <div class="form-group">
            <label for="kanbans">Kanban: </label>
            <?php if(isset($kanbans) && count($kanbans) > 0): ?>
                <select name="kanban" class="form-control">
                    <?php $__currentLoopData = $kanbans; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kanban): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($kanban->id); ?>"><?php echo e($kanban->name); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>
            <?php endif; ?>
        </div>
        <div class="form-group sr-only">
            <label for="users_email">User: </label>
            <select name="users_email" class="form-control">
                <option value="<?php echo e($user->email); ?>"><?php echo e($user->email); ?></option>
            </select>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </form>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('templates.template', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/public/bkanban2/Services/Application/Public/cad_task.blade.php ENDPATH**/ ?>