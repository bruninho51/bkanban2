<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="shortcut icon" href="/Services/Application/Public/Resources/Images/favicon.ico">
        <title><?php echo e($title); ?></title>
        <link rel="stylesheet" href="/Services/Application/Public/Resources/CSS/principal.css">
        <link rel="stylesheet" href="/Services/Application/Public/Lib/hint.css-2.5.1/hint.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="/Services/Application/Public/Lib/jQueryUI1.12.1/jquery-ui.css">
        <link rel="stylesheet" href="/Services/Application/Public/Lib/jQueryConfirm/jQueryConfirm.min.css">
        <link rel="stylesheet" href="/Services/Application/Public/Lib/bootstrap4.3.1/css/bootstrap.css">
        <link rel="stylesheet" href="/Services/Application/Public/Lib/ContextMenu/css/ContextMenu.css">
        <link rel="stylesheet" href="/Services/Application/Public/Lib/TaskEditor/css/TaskEditor.css">
        <link rel="stylesheet" href="/Services/Application/Public/Lib/TimerList/css/TimerList.css">
        <?php if(isset($stylesheets)): ?>
            <?php $__currentLoopData = $stylesheets; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $css): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <link rel="stylesheet" href="/Services/Application/Public/Resources/CSS/<?php echo e($css); ?>.css">
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>
    </head>
    <body>
        <?php echo $__env->yieldContent('navbar'); ?>
        
        <div class="container">
            <?php echo $__env->yieldContent('content'); ?>
        </div>

        <script src="/Services/Application/Public/Lib/jQuery/jQuery.js"></script>
        <script src="/Services/Application/Public/Lib/Moment/moment.js"></script>
        <script src="/Services/Application/Public/Lib/jQueryUI1.12.1/jquery-ui.js"></script>
        <script src="/Services/Application/Public/Lib/popper/Popper.js"></script>
        <script src="/Services/Application/Public/Lib/jQueryConfirm/jQueryConfirm.min.js"></script>
        <script src="/Services/Application/Public/Lib/bootstrap4.3.1/js/bootstrap.bundle.min.js"></script>
        <script src="/Services/Application/Public/Lib/bootstrap4.3.1/js/bootstrap.js"></script>
        <script src="/Services/Application/Public/Lib/jquery.nicescroll-3.7.6/jquery.nicescroll.min.js"></script>
        <script src="/Services/Application/Public/Lib/TimerList/TimerList.js"></script>
        <script src="/Services/Application/Public/Lib/Principal.js"></script>
        <script src="/Services/Application/Public/Lib/ContextMenu/ContextMenu.js"></script>
        <script src="/Services/Application/Public/Lib/TaskEditor/js/TaskEditor.js"></script>
        <script src="/Services/Application/Public/Lib/jQueryMask/jquery.mask.min.js"></script>
        <?php if(isset($scripts)): ?>
            <?php $__currentLoopData = $scripts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $js): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <script type="text/javascript" src="/Services/Application/Public/Lib/<?php echo e($js); ?>.js"></script>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>

        <?php if(isset($messages['alertSuccess'])): ?>
            <script>Principal.alert("Success", '<?php echo e(current($messages['alertSuccess'])); ?>', 'success')</script>
        <?php endif; ?>
        <?php if(isset($messages['alertDanger'])): ?>
            <script>Principal.alert("Error", '<?php echo e(current($messages['alertDanger'])); ?>', 'danger')</script>
        <?php endif; ?>
        <?php echo $__env->yieldContent('modal-timer-list'); ?>
    </body>
</html><?php /**PATH /var/www/public/bkanban2/Services/Application/Public/templates/template.blade.php ENDPATH**/ ?>