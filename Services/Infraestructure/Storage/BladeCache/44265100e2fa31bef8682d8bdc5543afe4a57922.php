<?php /* /var/www/public/bkanban2/Services/Application/Public/cad_user.blade.php */ ?>
<?php $__env->startSection('content'); ?>
    <form action="/users/register" method="post" class="container-form">
        <h2>Register User</h2>
        <div class="form-group">
            <label for="name">Name: </label>
            <input type="text" name="name" class="form-control">
        </div>
        <div class="form-group">
            <label for="email">Email: </label>
            <input type="email" name="email" class="form-control">
        </div>
        <div class="form-group">
            <label for="password">Password: </label>
            <input type="password" name="password" class="form-control">
        </div>
        <small class="form-text text-danger">
            <?php if(isset($messages['error'])): ?>
                <span class="error"><?php echo e(current($messages['error'])); ?></span><br>
            <?php endif; ?>
        </small>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </form>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('templates.template', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
