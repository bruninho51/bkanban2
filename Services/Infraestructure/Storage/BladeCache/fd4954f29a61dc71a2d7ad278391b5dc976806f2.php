<?php $__env->startSection('content'); ?>
    <h1 class="title"><?php echo e($title); ?></h1>
    <div class="filters">
        <?php echo $filters->mount(); ?>

    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('templates.template', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /**PATH /var/www/public/bkanban2/Services/Application/Public/base_report.blade.php ENDPATH**/ ?>