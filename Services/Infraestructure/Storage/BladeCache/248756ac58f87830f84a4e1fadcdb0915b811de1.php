<?php /* /var/www/public/bkanban2/Services/Application/Public/timer_list.blade.php */ ?>
<?php $__env->startSection('modal-timer-list'); ?>
    <div class="modal fade" id="modalTimerList" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Time List</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            Tempo Ocioso
                            <i class="list-clock fa fa-clock-o mr-3"></i>
                        </div>
                        <div class="card-footer text-muted d-flex justify-content-between">
                            <span class="time-today">05:00:00 hoje</span>
                            <button class="info-time-list btn btn-info">
                                <i class="fa fa-info-circle"></i>
                                Informations
                            </button>
                        </div>
                    </div>
                    <div class="card mt-2">
                        <div class="card-body">
                            Tempo Veficando Ordens de Serviço
                            <i class="list-clock fa fa-clock-o mr-3"></i>
                        </div>
                        <div class="card-footer text-muted d-flex justify-content-lg-between">
                            <span class="time-today">03:00:00 hoje</span>
                            <button class="info-time-list btn btn-info">
                                <i class="fa fa-info-circle"></i>
                                Informations
                            </button>
                        </div>
                    </div>
                </div>
                <!--<div class="modal-footer">
                </div>-->
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
