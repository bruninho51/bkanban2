<?php /* /var/www/public/bkanban2/Services/Application/Public/kanban.blade.php */ ?>
<?php $__env->startSection('kanban'); ?>
<ul class="nav nav-tabs" id="myTab" role="tablist">
    <?php $__currentLoopData = $kanbans; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kanban): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php if($loop->first): ?>
            <li class="nav-item">
                <a class="nav-link active" id="<?php echo e($kanban->getIdentity()); ?>-tab" data-toggle="tab" href="#<?php echo e($kanban->getIdentity()); ?>" role="tab" aria-controls="<?php echo e($kanban->getIdentity()); ?>" aria-selected="true"><?php echo e($kanban->name); ?></a>
            </li>
        <?php else: ?>
            <li class="nav-item">
                <a class="nav-link" id="<?php echo e($kanban->getIdentity()); ?>-tab" data-toggle="tab" href="#<?php echo e($kanban->getIdentity()); ?>" role="tab" aria-controls="<?php echo e($kanban->getIdentity()); ?>" aria-selected="false"><?php echo e($kanban->name); ?></a>
            </li>
        <?php endif; ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</ul>
<div class="tab-content" id="myTabContent">
    <?php $__currentLoopData = $kanbans; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kanban): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php if($loop->first): ?>
            <div class="tab-pane fade show active" data-idKanban="<?php echo e($kanban->id); ?>" id="<?php echo e($kanban->getIdentity()); ?>" role="tabpanel" aria-labelledby="<?php echo e($kanban->getIdentity()); ?>-tab">
                <?php echo $__env->make('tableau', ['kanban' => $kanban], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>
        <?php else: ?>
            <div class="tab-pane fade" data-idKanban="<?php echo e($kanban->id); ?>" id="<?php echo e($kanban->getIdentity()); ?>" role="tabpanel" aria-labelledby="<?php echo e($kanban->getIdentity()); ?>-tab">
                <?php echo $__env->make('tableau', ['kanban' => $kanban], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>
        <?php endif; ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>
<?php $__env->stopSection(); ?>