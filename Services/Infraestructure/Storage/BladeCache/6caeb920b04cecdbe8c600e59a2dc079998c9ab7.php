<?php /* /var/www/public/bkanban2/Services/Application/Public/tableau.blade.php */ ?>
<div class="row container-lists">
    <div class="col-md-4">
        <ul class="list">
            <h1>TO DO</h1>
            <div class="list-items to-do" data-code="1" id="<?php echo e($kanban->getIdentity()); ?>_to-do">
                <?php echo $__env->make('templates.listItems', ['tasks' => $kanban->tasks[1]], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>
        </ul>
    </div>

    <div class="col-md-4">
        <ul class="list">
            <h1>IN PROGRESS</h1>
            <div class="list-items in-progress" data-code="2" id="<?php echo e($kanban->getIdentity()); ?>_in-progress">
                <?php echo $__env->make('templates.listItems', ['tasks' => $kanban->tasks[2]], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>
        </ul>
    </div>

    <div class="col-md-4">
        <ul class="list">
            <h1>DONE</h1>
            <div class="list-items done" data-code="3" id="<?php echo e($kanban->getIdentity()); ?>_done">
                <?php echo $__env->make('templates.listItems', ['tasks' => $kanban->tasks[3]], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>
        </ul>
    </div>
</div>