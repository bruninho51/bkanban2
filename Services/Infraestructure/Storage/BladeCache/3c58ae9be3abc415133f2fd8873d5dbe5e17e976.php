<?php /* /var/www/public/bkanban2/Services/Application/Public/templates/listItems.blade.php */ ?>
<?php if(isset($tasks) && !empty($tasks)): ?>
    <?php $__currentLoopData = $tasks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $task): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <li data-code="<?php echo e($task->id); ?>" class="list-item" data-position="<?php echo e($task->position); ?>" data-color="<?php echo e($task->color); ?>" style="background-color: <?php echo e($task->color); ?>">
            <span>
                <strong data-toggle="popover" class="name" data-container="body" data-content="<?php echo e($task->name); ?>"><?php echo e($task->getNameResume()); ?></strong><br>
                <p data-toggle="popover" class="description" data-container="body" data-content="<?php echo e($task->description); ?>"><?php echo e($task->getDescriptionResume()); ?></p>
            </span>
            <i class="item-delete fa fa-close" onclick="Kanban.filedTask(this)"></i>
            <i class="item-clock fa fa-clock-o" onclick="TimerList.start(<?php echo e($task->id); ?>, 'tasks', '<?php echo e($task->getNameResume()); ?>')"></i>
        </li>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>