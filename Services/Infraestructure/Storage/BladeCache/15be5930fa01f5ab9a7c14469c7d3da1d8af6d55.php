<?php /* /var/www/public/bkanban2/Services/Application/Public/login.blade.php */ ?>
<?php $__env->startSection('content'); ?>
<form action="login" method="post" class="login-box">
    <img class="ico-login" src="/Services/Application/Public/Resources/Images/icon_black.png">
    <div class="form-group">
        <label for="email">E-mail:</label>
        <input type="email" name="email" class="form-control" id="email">
    </div>
    <div>
        <label for="password">Password:</label>
        <input type="password" name="password" class="form-control" id="password">
    </div>
    <small class="form-text text-danger">
        <?php if(isset($messages['error'])): ?>
            <span class="error"><?php echo e(current($messages['error'])); ?></span><br>
        <?php endif; ?>
    </small>
    <div class="form-group">
        <br>
        <button type="submit" class="btn btn-block btn-primary">Login</button>
    </div>
</form>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('templates.template', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>