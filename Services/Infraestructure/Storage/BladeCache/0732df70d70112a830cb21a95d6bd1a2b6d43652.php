<?php /* /var/www/public/bkanban2/Services/Application/Public/navbar.blade.php */ ?>
<?php $__env->startSection('navbar'); ?>

  <nav class="navbar navbar-light navbar-principal" style="background-color: #323A45;">
    <div class="container">
      <a href="/home" class="navbar-brand"><img src="/Services/Application/Public/Resources/Images/icon.png"></a>
      <div class="menu">
        <span class="navbar-toggle pr-4" data-toggle="collapse" data-target="#statisticsSubmenu">
          <a class="navLink" href="#">Statistics</a>
        </span>
        <span class="navbar-toggle pr-4" data-toggle="collapse" data-target="#usersSubmenu">
          <a class="navLink" href="#">Users</a>
        </span>
        <span class="navbar-toggle pr-4" data-toggle="collapse" data-target="#tasksSubmenu">
          <a class="navLink" href="#">Tasks</a>
        </span>
        <span class="navbar-toggle">
          <a href="/login/logoof">Sair</a>
        </span>
      </div>
      <div class="collapse navbar-collapse" id="statisticsSubmenu">
        <h3>Statistics</h3>
        <ul class="nav navbar-nav navbar-left">
          <li><a href="/timers/statistics/serviceTimeReport">Service Time Report</a>
        </ul>
      </div>
      <div class="collapse navbar-collapse" id="usersSubmenu">
        <h3>Users</h3>
        <ul class="nav navbar-nav navbar-left">
          <li><a href="/users/register">Register</a>
        </ul>
      </div>
      <div class="collapse navbar-collapse" id="tasksSubmenu">
        <h3>Tasks</h3>
        <ul class="nav navbar-nav navbar-left">
          <li><a href="/tasks/register">Register</a></li>
        </ul>
      </div>
    </div>
  </nav>
  <?php echo $__env->yieldContent('bar_utils'); ?>
<?php $__env->stopSection(); ?>