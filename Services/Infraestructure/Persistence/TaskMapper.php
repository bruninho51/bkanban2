<?php

namespace Services\Infraestructure\Persistence;

use Services\Infraestructure\Connection;
use Services\Domain\Task;
use Services\Domain\Kanban;
use \Exception;

class TaskMapper extends Mapper{
    
    const MODEL_NOT_SAVED = 'Task not saved.';
    const MODEL_GET_FAILURE = 'A failure occurred while getting the task';
    
    public static function save(Task $task)
    {
        if (!isset($task->users_email)) {
            $task->users_email = $task->user->email;
        }
        if (isset($task->kanban)) {
            $task->kanbans_id = $task->kanban->id;
        }
        if (!isset($task->color)) {
            $task->color = Task::COLOR_DEFAULT;
        }

        if ($task->position == 0) {
            $task->position = TaskMapper::getLastPosition() + 1;
            return parent::saveModel($task);
        }

        //Iterar sobre as tarefas com position >= a $task->position
        //Faz isso para reorganizar as tarefas
        $prevTasks = self::findByWhere([
            0 => ['key' => 'position', 'operator' => '>=', 'value' => $task->position],
            1 => ['key' => 'list', 'operator' => '=', 'value' => $task->list]
        ]);
        foreach ($prevTasks as $prevTask) {
            $prevTask->position++;
            parent::saveModel($prevTask);
        }

        return parent::saveModel($task);
    }
    
    public static function findLast() : Task
    {
        return parent::findLastModel(new Task);
    }
    
    public static function findLastId() : int
    {
        return parent::findLastModelId(new Task);
    }

    public static function findByWhere($where, $orderBy = []) : array
    {
        return parent::findModelByWhere(new Task, $where, $orderBy);
    }

    public static function findById($id) : Task
    {
        try {
            return parent::findModelById($id, new Task);
        } catch (\Exception $ex) {
            throw new Exception(self::MODEL_GET_FAILURE);
        }
    }

    public static function findTasksForList(int $list)
    {
        return parent::findModelByWhere(new Task, [
            0 => ['key' => 'filed', 'value' => '0'],
            1 => ['key' => 'list', 'value' => $list],
            2 => ['key' => 'users_email', 'value' => $_SESSION['user']->email]
        ], ['orderBy' => 'position', 'type' => 'ASC']);
    }
    
    public static function getLastPosition()
    {
        return parent::findModelPropMax(new Task, 'position');
    }
    
    public static function getTasksPositionByList(int $list)
    {
        $tasks = parent::findModelByWhere(new Task, [
            0 => ['key' => 'filed', 'value' => '0'],
            1 => ['key' => 'list', 'value' => $list]
        ], ['orderBy' => 'position', 'type' => 'ASC']);
        
        $data = [];
        
        foreach ($tasks as $task) {
            $data[$task->id] = $task->position;
        }
        
        return $data;
    }

    public static function findAll($withDeleted = false)
    {
        return parent::findAllModel(new Task, $withDeleted);
    }

    public static function findByKanban(Kanban $kanban)
    {
        $tasks = self::findByWhere([
            0 => ['key' => 'filed', 'value' => '0'],
            1 => ['key' => 'kanbans_id', 'value' => $kanban->id]
        ], [], ['orderBy' => 'position', 'type' => 'ASC']);

        $data = [];
        
        foreach ($tasks as $task) {
            $data[$task->id] = $task;
        }
        
        return $data;
    }
}
