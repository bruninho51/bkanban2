<?php

namespace Services\Infraestructure\Persistence;

use Services\Domain\User;
use Services\Infraestructure\Connection;
use Services\Domain\Kanban;
use \Exception;

class KanbanMapper extends Mapper{
    
    const MODEL_NOT_SAVED = 'Kanban not saved.';
    const MODEL_GET_FAILURE = 'A failure occurred while getting the kanban';

    public static function save(Kanban $kanban)
    {
        return parent::saveModel($kanban);
    }
    
    public static function findLast() : Kanban
    {
        return parent::findLastModel(new Kanban);
    }
    
    public static function findLastId() : int
    {
        return parent::findLastModelId(new Kanban);
    }

    public static function findByWhere($where) : array
    {
        return parent::findModelByWhere(new Kanban, $where);
    }

    public static function findById($id) : Kanban
    {
        try {
            return parent::findModelById($id, new Kanban);
        } catch (\Exception $ex) {
            throw new Exception(self::MODEL_GET_FAILURE);
        }
    }

    public static function findAll($withDeleted = false)
    {
        return parent::findAllModel(new Kanban, $withDeleted);
    }

    public static function findAllWithTasks($withDeleted = false)
    {
        $kanbans = self::findAll($withDeleted);
        foreach ($kanbans as &$kanban) {
            $lists = [
                1 => [], //TO DO
                2 => [], //In Progress
                3 => []  //Done
            ];
            $tasks = TaskMapper::findByKanban($kanban);
            foreach ($tasks as $task) {
                $lists[$task->list][] = $task;
            }
            $kanban->tasks = $lists;
        }

        return $kanbans;
    }

    public static function findWithTasksByUser(User $user, $withDeleted = false)
    {
        $where[] = [
            'key' => 'deletedAt',
            'operator' => $withDeleted ? 'is not' : 'is',
            'value' => 'null'
        ];
        $where[] = ['key' => 'users_email', 'value' => $user->email];

        $kanbans = self::findByWhere($where);
        foreach ($kanbans as &$kanban) {
            $lists = [
                1 => [], //TO DO
                2 => [], //In Progress
                3 => []  //Done
            ];
            $tasks = TaskMapper::findByKanban($kanban);
            foreach ($tasks as $task) {
                $lists[$task->list][] = $task;
            }
            $kanban->tasks = $lists;
        }

        return $kanbans;
    }
}
