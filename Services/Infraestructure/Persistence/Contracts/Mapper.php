<?php

namespace Services\Infraestructure\Persistence\Contracts;

use Services\Domain\Model;
interface Mapper {
    
    public static function saveModel(Model $model) : Model;
    public static function findLastModel(Model $model) : Model;
    public static function findLastModelId(Model $model) : int;
    public static function findModelByWhere(Model $model, array $filter) : array;
    public static function findAllModel(Model $model) : array;
    public static function findModelById($modelId, Model $model) : Model;
}
