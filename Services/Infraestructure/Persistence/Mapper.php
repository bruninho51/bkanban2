<?php

namespace Services\Infraestructure\Persistence;

use Services\Infraestructure\Connection;
use Services\Infraestructure\Persistence\Contracts\Mapper as MapperInterface;
use Services\Domain\Model;
use \PDO;
use \DateTime;
use Exception;

abstract class Mapper implements MapperInterface{

    const ERROR_GETTING_DATA = 'An error occurred while trying to get the data. Try again later! If the problem persists, contact your administrator.';

    private static function getbindParam(Model $model)
    {
        $fillableQuery = self::getFillableQuery($model);
        return array_map(function ($field) {
            return ':' . $field;
        }, $fillableQuery);
    }

    private static function getFillableQuery(Model $model)
    {
        $fillableQuery = [];
        $fillable = $model->getFillable();
        foreach ($fillable as $field) {
            if (!in_array($field, $model->getOptionalFields()) || isset($model->$field)) {
                $fillableQuery[] = $field;
            }
        }

        return $fillableQuery;
    }

    private static function getSetParam(Model $model)
    {
        $fillableQuery = self::getFillableQuery($model);
        return array_map(function ($field) {
            return $field . ' = ' . ':' . $field;
        }, $fillableQuery);
    }

    private static function updateModel(Model $model) : Model
    {
        if (in_array('updatedAt', $model->getFillable())) {
            $model->updatedAt = strtotime(date('Y-m-d H:i:s'));
        }

        $arrSetParam   = self::getSetParam($model);
        $fillableQuery = self::getFillableQuery($model);
        $setParam = implode(', ', $arrSetParam);
        $sql = 'UPDATE ' . $model::TABLE . ' SET ' . $setParam;
        /**
         * Alterar depois, para que todas as tabelas não tenham
         * que ter obrigatoriamente o campo id como chave primária.
         */
        $sql .= ' WHERE id = ' . $model->id;

        $stmt = Connection::get()->prepare($sql);

        for ($i = 0; $i < count($arrSetParam); $i++) {
            $varName = $fillableQuery[$i];
            /*
             * @ é para omitir uma notice, pois segundo parâmetro deseja receber
             * uma referência, e o método mágico __get passa apenas o valor, não
             * a referência para a propriedade
             */
            @$stmt->bindParam(':'.$varName, $model->$varName);
        }

        $updated = $stmt->execute();

        Connection::close();

        if ($updated) {
            return self::findModelById($model->id, $model);
        }

        throw new Exception(self::MODEL_NOT_SAVED);
    }
    
    public static function saveModel(Model $model) : Model
    {
        try {
            self::findModelById($model->id, $model);
            return self::updateModel($model);
        } catch (Exception $ex) {
            if (in_array('createdAt', $model->getFillable())) {
                $model->createdAt = strtotime(date('Y-m-d H:i:s'));
            }

            $arrBindParam = self::getbindParam($model);
            $fillableQuery = self::getFillableQuery($model);
            $bindParam = implode(', ', $arrBindParam);
            $fields = implode(', ', $fillableQuery);

            $sql = 'INSERT INTO ' . $model::TABLE . "({$fields})" .
                "VALUES({$bindParam})";
            $stmt = Connection::get()->prepare($sql);

            for ($i = 0; $i < count($arrBindParam); $i++) {
                $varName = $fillableQuery[$i];
                /*
                 * @ é para omitir uma notice, pois segundo parâmetro deseja receber
                 * uma referência, e o método mágico __get passa apenas o valor, não
                 * a referência para a propriedade
                 */
                @$stmt->bindParam($arrBindParam[$i], $model->$varName);
            }

            $saved = $stmt->execute();

            Connection::close();

            if ($saved) {
                return self::findLastModel($model);
            }
        }
        
        throw new Exception(self::MODEL_NOT_SAVED);
    }
    
    public static function findLastModel(Model $model) : Model
    {
        $sql = 'SELECT * FROM ' . $model::TABLE . ' ORDER BY 1 DESC LIMIT 1';
        $model = Connection::get()->query($sql)->fetchObject(get_class($model));
        
        Connection::close();
        
        if ($model) {
            return $model;
        }
        
        throw new Exception(self::MODEL_GET_FAILURE);
    }
    
    public static function findLastModelId(Model $model) : int
    {
        $sql = 'SELECT MAX(id) AS id FROM ' . $model::TABLE;
        $id = Connection::get()->query($sql)->fetch(PDO::FETCH_OBJECT)->id;
        
        Connection::close();
        
        return $id;
    }
    
    public static function findModelByWhere(Model $model, array $filter, array $joins = [], array $orderBy = []) : array
    {
        $arrFieldsWithTable = array_map(function ($field) use($model) {
            $table = $model::TABLE;
            return "{$table}.{$field}";
        }, $model->getFieldsToShow());
        $fields = implode(', ', $arrFieldsWithTable);

        $sql = 'SELECT ' . $fields . ' FROM ' . $model::TABLE;

        foreach ($joins as $join) {
            $sql .= $join;
        }

        $i = 0;
        $sql .= ' WHERE ';
        foreach ($filter as $where) {
            if ($i > 0) {
                $sql .= " AND ";
            }

            $operator = '=';
            if (isset($where['operator'])) {
                $operator = $where['operator'];
            }

            $valueWhere = $where['value'];
            self::processValuesWhereClause($valueWhere);

            switch ($operator) {
                case 'between':
                    $sql .= " ({$where['key']} {$operator} '{$valueWhere[0]}' AND '{$valueWhere[1]}') ";
                    break;
                default:
                    $valueWhere = $valueWhere == 'null' ? $valueWhere : "'{$valueWhere}'";
                    $sql .= " {$where['key']} {$operator} {$valueWhere} ";
            }
            $i++;
        }
        if ($orderBy) {
            $sql .= 'ORDER BY ' . $orderBy['orderBy'] . ' ' . $orderBy['type'];
        }

        $res = Connection::get()->query($sql)->fetchAll(PDO::FETCH_CLASS, get_class($model));

        if ($res === false) {
            throw new Exception($model::MSG_DATA_MISING);
        }
        
        Connection::close();
        
        return $res;
    }

    private static function processValuesWhereClause(&$values)
    {
        // Valores DateTime são transformados em timestamp
        if (is_array($values)) {
            array_walk_recursive($values, function (&$value) {
                if ($value instanceof DateTime) {
                    $value = $value->getTimestamp();
                }
            });
        } elseif ($values instanceof DateTime) {
            $values = $values->getTimestamp();
        }
    }

    public static function findAllModel(Model $model, $withDeleted = false) : array
    {
        $fieldsToShow = implode(', ', $model->getFieldsToShow());
        $len = strlen($fieldsToShow);

        $sql = "SELECT {$fieldsToShow} FROM " . $model::TABLE;

        if (!$withDeleted && in_array('deletedAt', $model->getFillable())) {
            $sql .= " WHERE deletedAt IS NULL";
        }

        $stmt = Connection::get()->prepare($sql);
        $stmt->execute();

        $res = $stmt->fetchAll(PDO::FETCH_CLASS, get_class($model));

        Connection::close();

        if ($res === false) {
            throw new Exception(self::ERROR_GETTING_DATA);
        }

        return $res;
    }

    public static function findModelById($modelId, Model $model) : Model
    {
        $data = current(self::findModelByWhere($model, [0 => ['key' => 'id', 'value' => $modelId]]));
        if ($data === false) {
            throw new Exception(self::ERROR_GETTING_DATA);
        }

        return $data;
    }
    
    public static function findModelPropMax(Model $model, $prop)
    {
        $sql = 'SELECT MAX(' . $prop . ') AS max FROM ' . $model::TABLE;
        
        $res = Connection::get()->query($sql)->fetch(PDO::FETCH_OBJ);
        
        Connection::close();
        
        return $res->max;
    }

    public static function join($table, $exp1, $exp2)
    {
        return " JOIN {$table} ON {$exp1} = {$exp2} ";
    }
}
