<?php

namespace Services\Infraestructure\Persistence;

use Services\Infraestructure\Connection;
use Services\Domain\User;

class UserMapper extends Mapper{
    
    const MODEL_NOT_SAVED = 'User not saved.';
    const MODEL_GET_FAILURE = 'A failure occurred while getting the user';
    
    public static function save(User $user) : User
    {
        return parent::saveModel($user);
    }
    
    public static function findLast() : User
    {
        return parent::findLastModel(new User);
    }
    
    public static function findLastId() : int
    {
        return parent::findLastModelId(new User);
    }
    
    public static function findByWhere(array $filter)
    {
        return parent::findModelByWhere(new User, $filter);
    }

    public static function findAll()
    {
        return parent::findAllModel(new User);
    }

    public static function findByEmail(String $email) : User
    {
        return self::findByWhere(['email' => $email]);
    }
}
