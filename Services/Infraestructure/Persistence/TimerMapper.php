<?php


namespace Services\Infraestructure\Persistence;

use Services\Domain\Model;
use Services\Domain\Timer;
use Services\Domain\User;

class TimerMapper extends Mapper
{
    const MODEL_NOT_SAVED = 'Timer not saved';
    const MODEL_GET_FAILURE = 'A failure occurred while getting the timer registry';

    public static function save(Timer $timer) : Timer
    {
        return parent::saveModel($timer);
    }

    public static function findLast() : Timer
    {
        return parent::findLastModel();
    }

    public static function findLastId() : int
    {
        return parent::findLastModelId(new Timer);
    }

    public static function findByWhere(array $filter, $joins = [])
    {
        return parent::findModelByWhere(new Timer, $filter, $joins);
    }

    public static function findAll()
    {
        return parent::findAllModel(new Timer);
    }

    public static function findByUser(User $user, $where)
    {
        $where[] = ['key' => 'tasks.users_email', 'value' => $user->email];
        $joins[] = Mapper::join('tasks', 'tasks.id', 'timers.foreign_key');

        return self::findByWhere($where, $joins);
    }
}